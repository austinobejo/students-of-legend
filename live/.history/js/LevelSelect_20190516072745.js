class LevelButton {
	constructor(x, y, key, callback, callbackContext, overFrame, outFrame, downFrame, upFrame,
		text, textStyle, xOffset, yOffset, locked) {
		this.game = callbackContext.game;
		this.buttonImage = this.game.add.button(x, y, key, callback, callbackContext, overFrame,
			outFrame, downFrame, upFrame);
		this.buttonText = this.game.add.text(x + xOffset, y + yOffset, text, textStyle);
		if (locked) {
			this.lock = this.game.add.image(x, y, 'lock');
		}
	}
}

class LevelSelect {
	constructor() {

	}
	preload() {
		this.game.load.image('background', 'resources/ui/PageBackground.jpg');
		this.game.load.image('lock', 'resources/ui/lock.png');
		this.game.load.spritesheet('levelBtn', 'resources/ui/level_button_sprite_sheet.png', 294, 294);
	}

	create() {

		var bg;
		bg = this.game.add.image(0, 0, 'background');
		bg.width = window.innerWidth;
		bg.height = window.innerHeight;

		// var btnStyle = { font: "Cinzel", fontSize: "36px", fontWeight: "bold", fill: "#2e2e2e" };
		// var tutorialButton = new MenuButton(0, 0, 'levelBtn',
		// 	function () {
		// 		this.game.state.start('tutorial');
		// 	},
		// 	this, 1, 0, 2, 0, Tutorial, btnStyle, 0, 0, false)

		var buttonImage = this.game.add.button(0, 0, 'levelBtn', function () {
			this.game.state.start('tutorial');
		}
			, this, 1, 0, 2, 0);

		buttonImage.centerX = this.game.world.centerX;
		buttonImage.centerY = this.game.world.centerY / 2;

		var style = { font: "36px Cinzel", fill: "#000000", align: "center" };
		var buttonText = this.game.add.text(0, 0, "Tutorial", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.centerY = buttonImage.centerY;


		var buttonImage = this.game.add.button(0, 0, 'levelBtn', function () {
			this.game.state.start('level1');
		}
			, this, 1, 0, 2, 0);

		buttonImage.centerX = this.game.world.centerX;
		buttonImage.centerY = (this.game.world.centerY + this.game.world.height) / 2;

		var buttonText = this.game.add.text(0, 0, "Level 1", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.centerY = buttonImage.centerY;


		var buttonImage = this.game.add.button(0, 0, 'levelBtn', function () {
			this.game.state.start('credits');
		}
			, this, 1, 0, 2, 0);

		buttonImage.centerX = this.game.world.centerX / 2;
		buttonImage.centerY = this.game.world.centerY;

		var buttonText = this.game.add.text(0, 0, "Credits", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.centerY = buttonImage.centerY;

		var buttonImage = this.game.add.button(0, 0, 'levelBtn', function () {
			this.game.state.start('controls');
		}
			, this, 1, 0, 2, 0);

		buttonImage.centerX = (this.game.world.centerX + this.game.world.width) / 2;
		buttonImage.centerY = this.game.world.centerY;

		var buttonText = this.game.add.text(0, 0, "Controls", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.centerY = buttonImage.centerY;
		// var btnList = [];
		// var btnX = 200;
		// var btnY = 250;
		// var btnInc = 300;
		// var btnTxtXBase = 50;
		// var btnTxtY = 30;


		// var btnXStart = btnX;
		// for (const [btnText, btnProp] of Object.entries(btnMap)) {
		// 	var btnTxtX = btnTxtXBase / btnText.length;
		// 	btnList.push();
		// 	btnXStart = btnXStart + btnInc;
		// }
	}
}

class Controls {
	constructor() {

	}
	preload() {
		this.game.load.image('background', 'resources/ui/PageBackground.jpg');
		this.game.load.spritesheet('button', 'resources/ui/button_sprite_sheet.png', 623, 104);
	}


	create() {
		var bg = this.game.add.image(0, 0, 'background');
		bg.width = window.innerWidth;
		bg.height = window.innerHeight;

		var buttonImage = this.game.add.button(0, 0, 'button', function () {
			this.game.state.start('levelSelect');
		}
			, this, 1, 0, 2, 0);
		buttonImage.centerX = this.game.world.centerX;
		buttonImage.centerY = (this.game.world.centerY + this.game.world.height) / 2;

		var style = { font: "36px Cinzel", fill: "#000000", align: "center" };
		var buttonText = this.game.add.text(0, 0, "Back", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.centerY = buttonImage.centerY;

		var height = buttonImage.top;

		buttonText = this.game.add.text(0, 0, "(Cheat) InstaKill: Letter K", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.bottom = height;
		height -= buttonText.height;

		buttonText = this.game.add.text(0, 0, "(Cheat) Invinisibility: Letter I", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.bottom = height;
		height -= buttonText.height;

		buttonText = this.game.add.text(0, 0, "Movement: WASD", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.bottom = height;
		height -= buttonText.height;

		buttonText = this.game.add.text(0, 0, "Attacking: Arrow Keys", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.bottom = height;
		height -= buttonText.height;
	}
}

class Credits {
	constructor() {

	}
	preload() {
		this.game.load.image('background', 'resources/ui/PageBackground.jpg');
		this.game.load.spritesheet('button', 'resources/ui/button_sprite_sheet.png', 623, 104);
	}
	create() {
		var bg = this.game.add.image(0, 0, 'background');
		bg.width = window.innerWidth;
		bg.height = window.innerHeight;

		var buttonImage = this.game.add.button(0, 0, 'button', function () {
			this.game.state.start('levelSelect');
		}
			, this, 1, 0, 2, 0);
		buttonImage.centerX = this.game.world.centerX;
		buttonImage.centerY = (this.game.world.centerY + this.game.world.height) / 2;

		var style = { font: "36px Cinzel", fill: "#000000", align: "center" };
		var buttonText = this.game.add.text(0, 0, "Back", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.centerY = buttonImage.centerY;


		buttonText = this.game.add.text(0, 0, "Sounds, Character Assets, Level Design, ect\nAustin Joseph", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.bottom = this.game.world.height * .5;

		buttonText = this.game.add.text(0, 0, "Ui Art: Joseph Pelligrino", style);
		buttonText.centerX = buttonImage.centerX;
		buttonText.bottom = this.game.world.height * .25;
	}
}

class DeathScreen {
	//Two buttons retry or main menu 
}