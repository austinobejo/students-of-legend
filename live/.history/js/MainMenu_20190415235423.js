class MenuButton {
	constructor(x, y, key, callback, callbackContext, overFrame, 
			    outFrame, downFrame, upFrame, text, textStyle, xOffset, yOffset) {
		this.game = callbackContext.game;
		this.buttonImage = this.game.add.button(x, y, key, callback, callbackContext, overFrame, 
			    outFrame, downFrame, upFrame);
		this.buttonText = this.game.add.text(x+xOffset, y+yOffset, text, textStyle);
	}
}

class MainMenu {
	constructor() {
		this.preload = function() {
			this.game.load.image('background', 'resources/PageBackground.jpg');
			this.game.load.spritesheet('button', 'resources/button_sprite_sheet.png', 623, 104);
		}

		var bg;
		var font = "Cinzel";
		var font_color = "#2e2e2e";
		const btnMap = {
			"New Game": newGame,
			"Continue": continueFunc,
			"Controls":null,
			"About":null,
		};
		var btnList = [];
		var btnX = 370;
		var btnY = 250;
		var btnInc = 100;
		var btnTxtXBase = 50;
		var btnTxtY = 30;
		var btnStyle = { font: font, fontSize: "36px", fontWeight:"bold", fill: font_color };
		this.create = function() {
			bg = this.game.add.image(0, 0, 'background');
    		bg.width = window.innerWidth;
   			bg.height = window.innerHeight;

   			var btnYStart = btnY;
			for(const [btnText, onClickFunction] of Object.entries(btnMap)) {
				var btnTxtX = btnTxtXBase / btnText.length;
				btnList.push(new MenuButton(btnX, btnYStart, 'button', onClickFunction, this, 
										   1, 0, 2, 0, btnText, btnStyle, btnTxtX, btnTxtY));
				btnYStart = btnYStart + btnInc;
			}
		}
		this.update = function() {

		}

		function newGame() {
			this.game.state.start('level0');
		}
		function continueFunc() {
			this.game.state.start('levelSelect');
		}
	}
}