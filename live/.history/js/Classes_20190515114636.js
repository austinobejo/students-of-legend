class Grid {
    constructor() {
        this.grid = [];
    }

    setPlayer(player) {

        this.player = player;
        this.player.sprite.centerX = this.sprite.centerX;
        this.player.sprite.centerY = this.sprite.centerY;
        this.grid.push(this.player);
    }
    update() {
        this.player.parseInput();
        this.updatePositions();
    }
    updatePositions() {
        var cellWidth = this.sprite.width / 5.0;
        var cellHeight = this.sprite.height / 5.0;

        for (var i = 0; i < this.grid.length; i++) {
            var current = this.grid[i];
            if (current.onBoard) {
                current.sprite.centerX = this.sprite.centerX + (cellWidth * current.gridX);
                current.sprite.centerY = this.sprite.centerY + (cellHeight * current.gridY);
            }
        }
    }
}

class GridObject {
    constructor() {
        this.moveTo(0, 0);
        this.onBoard = false;
    }
    moveTo(x, y) {
        this.gridX = x;
        this.gridY = y;
    }
}

class Player extends GridObject {

    constructor() {
        super();
        this.onBoard = true;
    }
    parseInput() {

        var x = 0;
        var y = 0;

        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.W)) {
            y -= 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.S)) {
            y += 1;
        }
        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            x -= 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
            x += 1;
        }
        this.moveTo(x, y);
        this.up = this.level.game.input.keyboard.isDown(Phaser.Keyboard.UP);
        this.down = this.level.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        this.left = this.level.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        this.right = this.level.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
    }
}

class Boss {

    create() {

    }
}

class King extends Boss {

    create() {

        this.pawns = new Array();
        this.bishops = new Array();
        this.pawnWidth = null;

        for (var i = 0; i < 4; i++) {
            var pawn = new ChessPiece();
            pawn.sprite = this.level.game.add.sprite(0, 0, "pawn_base");
            this.pawns.push(pawn);
            if(this.pawnWidth == null) {
                this.pawnWidth = pawn.sprite.width;
            }
        }
    }
    update() {
        this.updatePositions();

    }
    updatePositions() {

        var start  = this.sprite.centerX - (this.pawnWidth * this.pawns.length);    
        for (var i = 0; i < this.pawns.length; i++) {
            var pawn = pawns[i];
            this.pawn.centerX = start + (this.pawnWidth * i);
            this.pawn.centerY = this.sprite.centerY + pawn.sprite.height;

        }
    }
}

class ChessPiece extends GridObject {

}