class Grid {
    constructor() {
        this.grid = [];
    }

    setPlayer(player) {

        this.player = player;
        this.player.sprite.centerX = this.sprite.centerX;
        this.player.sprite.centerY = this.sprite.centerY;
        this.grid.push(this.player);
    }
    update() {
        this.player.parseInput();
        this.updatePositions();
    }
    updatePositions() {
        var cellWidth = this.sprite.width / 5.0;
        var cellHeight = this.sprite.height / 5.0;

        for (var i = 0; i < this.grid.length; i++) {
            var current = this.grid[i];
            if (current.onBoard) {
                current.sprite.centerX = this.sprite.centerX + (cellWidth * current.gridX);
                current.sprite.centerY = this.sprite.centerY + (cellHeight * current.gridY);
            }
        }
    }
}

class GridObject {
    constructor() {
        this.moveTo(0, 0);
        this.onBoard = false;
    }
    moveTo(x, y) {
        this.gridX = x;
        this.gridY = y;
    }
}

class Player extends GridObject {

    constructor() {
        super();
        this.onBoard = true;
    }
    parseInput() {

        var x = 0;
        var y = 0;

        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.W)) {
            y -= 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.S)) {
            y += 1;
        }
        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            x -= 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
            x += 1;
        }
        this.moveTo(x, y);
        this.up = this.level.game.input.keyboard.isDown(Phaser.Keyboard.UP);
        this.down = this.level.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        this.left = this.level.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        this.right = this.level.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
    }
}

class Boss {

    constructor() {
        this.phase = 0;
        this.nextDetected = false;
    }
    create() {

    }
    update() {

    }
}

class TutorialBoss extends Boss {

    create() {
        // var text = this.level.game.add.bitmapText(400, 300, 'desyrel', 'Middle Earth', 64);
        // text.anchor.x = 0.5;
        // text.anchor.y = 0.5;
        this.dialog = ["I am the black king, the original ruler of this land.\nYour task is to become my successor by conquring\nthe challenges laid before you.\nGood Luck",
            "Your two most important abilities are\nMoving by using WASD and\nAttacking by using the Arrow Keys",
            "You can move within the lighter squares\nwhile your enemies may move within the darker squares", "If you'd like to test your new abilties move to\nthe top right square and attack the pawn."
            , "When an enemy is about to attack the\nthreatened squares will have flashing\nblue boxes on them as shown here.",
            "You will learn the way each piece attack in time",
            "Should you fail to dodge an attack you will take\ndamage, you have 3 chances, be careful lest\nyou lose them all",
            "Finally a piece will have a blue outline when it\nis vulnerable and a grey outline when it is\ninvulnerable use this to your advantage to\navoid putting yourself in danger.",
            "You are now ready to earn the title of king."
        ]
        var style = { font: "40px Calibri", fill: "#000000", align: "center" };
        this.text = this.level.game.add.text(0, 0, this.dialog[this.phase], style);
        this.text.centerX = this.sprite.centerX;
        this.text.centerY = this.sprite.bottom + this.text.height/2;

        var spaceBarText = this.level.game.add.text(0, 0, "Press [SPACE] to advance", style);
        spaceBarText.centerX = this.sprite.centerX;
        spaceBarText.y = 0 + 5;
    }

    update() {
        var down = this.level.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR);
        if (down) {
            if (!this.nextDetected) {

                this.showNextText();
                this.nextDetected = true;
            }
        } else {
            this.nextDetected = false;
        }
    }

    showNextText() {

        if (this.phase >= this.dialog.length) {

            this.text.text = "Maint Menu";
            //Go to main menu
        } else {
            this.phase = this.phase + 1;

            this.text.text = this.dialog[this.phase];
            this.text.centerX = this.sprite.centerX;
            this.text.centerY = this.sprite.bottom + this.text.height/2;
        }
    }
}
class King extends Boss {

    create() {

        this.pawns = new Array();
        this.bishops = new Array();
        this.pawnWidth = null;

        for (var i = 0; i < 8; i++) {
            var pawn = new ChessPiece();
            pawn.sprite = this.level.game.add.sprite(0, 0, "pawn_base");
            this.pawns.push(pawn);
            if (this.pawnWidth == null) {
                this.pawnWidth = pawn.sprite.width;
            }
        }
    }
    update() {
        this.updatePositions();
    }
    updatePositions() {
        var start = this.sprite.centerX - ((this.pawnWidth * this.pawns.length) / 2);
        for (var i = 0; i < this.pawns.length; i++) {
            var pawn = this.pawns[i];
            pawn.sprite.centerX = start + (this.pawnWidth * i);
            pawn.sprite.centerY = this.sprite.bottom + pawn.sprite.height;
        }
    }
}

class ChessPiece extends GridObject {

}