class Grid {
    constructor() {
        this.grid = [];
    }

    setPlayer(player) {

        this.player = player;
        this.player.sprite.centerX = this.sprite.centerX;
        this.player.sprite.centerY = this.sprite.centerY;
        this.grid.push(this.player);
    }
    update() {
        this.player.parseInput();
        this.updatePositions();
    }
    updatePositions() {
        var cellWidth = this.sprite.width / 5.0;
        var cellHeight = this.sprite.height / 5.0;

        for (var i = 0; i < this.grid.length; i++) {
            var current = this.grid[i];
            current.sprite.centerX = this.sprite.centerX + (cellWidth * current.gridX);
            current.sprite.centerY = this.sprite.centerY + (cellHeight * current.gridX);
            console.log(cellWidth + " " + cellHeight)
        }
    }
}

class GridObject {
    moveTo(x, y) {
        this.gridX = x;
        this.gridY = y;
    }
}

class Player extends GridObject {

    constructor() {
        super();
        moveTo(0, 0);
    }
    parseInput() {

        var x = 0;
        var y = 0;

        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.W)) {
            y += 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.S)) {
            y -= 1;
        }
        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            x -= 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
            x += 1;
        }

        this.up = this.level.game.input.keyboard.isDown(Phaser.Keyboard.UP);
        this.down = this.level.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        this.left = this.level.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        this.right = this.level.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
        console.log(x + " " + y)
    }
}