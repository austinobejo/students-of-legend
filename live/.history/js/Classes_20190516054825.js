class Grid {
    constructor() {
        this.grid = new Array();
    }
    addPiece(piece) {
        if (piece.onBoard) {
            piece.sprite.centerX = this.sprite.centerX + (this.cellWidth * piece.gridX);
            piece.sprite.centerY = this.sprite.centerY + (this.cellHeight * piece.gridY);
        }
        this.grid.push(piece);
    }
    removePiece(piece) {
        var foundIndex = null;

        for (var i = 0; i < this.grid.length && foundIndex == null; i++) {

            if (this.grid[i] == piece) {
                foundIndex = i
            }
        }
        if (foundIndex != null) {
            this.grid.splice(foundIndex);
        }
    }
    setPlayer(player) {
        this.player = player;
        this.addPiece(player);
    }
    create() {
        this.cellWidth = this.sprite.width / 5.0;
        this.cellHeight = this.sprite.height / 5.0;
    }
    update() {
        var deadThings = new Array();
        for (var i = 0; i < this.grid.length; i++) {
            var current = this.grid[i];
            if (current.isDead()) {
                deadThings.push(current)
            } else if (current.onBoard) {
                current.sprite.centerX = this.sprite.centerX + (this.cellWidth * current.gridX);
                current.sprite.centerY = this.sprite.centerY + (this.cellHeight * current.gridY);
                // current.sprite.bringToTop();
            }
        }

        for (var i = 0; i < deadThings.length; i++) {
            this.removePiece(deadThings[i]);
        }
    }
    getGridCoords(x, y) {

        return [this.sprite.centerX + (this.cellWidth * x), this.sprite.centerY + (this.cellHeight * y)]
    }
    toGridCoords(sprite) {

        return [(sprite.centerX - this.sprite.centerX) / this.cellWidth, (sprite.centerY - this.sprite.centerY) / this.cellHeight]
    }
    isEmpty(x, y) {
        var count = 0;
        for (var i = 0; i < this.grid.length; i++) {
            var current = this.grid[i];
            if (current.gridX == x && current.gridY == y) {
                count++;
            }
        }
        return count == 0;
    }
    dealDamage(x, y, damage) {

        for (var i = 0; i < this.grid.length; i++) {
            var current = this.grid[i];
            if (current.gridX == x && current.gridY == y) {
                current.takeDamage(damage);
            }
        }
    }
}
class GridObject {
    constructor() {
        this.gridX = 0;
        this.gridY = 0;
        this.onBoard = false;
        this.damage = 0;
        this.health = 10;
        this.busy = false;
    }

    isDead() {
        return this.health <= 0;
    }
    moveTo(x, y) {
        this.gridX = x;
        this.gridY = y;
        this.onBoard = true;
    }
    removeFromBoard() {
        this.onBoard = false;
    }
    takeDamage(amount) {

        this.health -= Math.min(this.health, amount);
        if (this.isDead()) {
            this.onDeath();
        }
    }

    onDeath() {

    }
}
class Player extends GridObject {

    constructor() {
        super();
        this.moveTo(0, 0);
        this.attackPhase = -1;
        this.damage = 1;
        this.health = 3;
    }
    update() {

        if (this.health <= 0) {
            this.sprite.visible = false;
            this.damaged1Sprite.visible = false;
            this.damaged2Sprite.visible = false;
            this.deadSprite.centerX = this.sprite.centerX;
            this.deadSprite.centerY = this.sprite.centerY;
            this.deadSprite.visible = true;
        } else if (this.health == 1) {
            this.sprite.visible = false;
            this.damaged1Sprite.visible = false;
            this.deadSprite.visible = false;
            this.damaged2Sprite.centerX = this.sprite.centerX;
            this.damaged2Sprite.centerY = this.sprite.centerY;
            this.damaged2Sprite.visible = true;
        } else if (this.health == 2) {

            this.sprite.visible = false;
            this.damaged2Sprite.visible = false;
            this.deadSprite.visible = false;
            this.damaged1Sprite.centerX = this.sprite.centerX;
            this.damaged1Sprite.centerY = this.sprite.centerY;
            this.damaged1Sprite.visible = true;
        } else if (this.health >= 3) {

            this.sprite.visible = true;
            this.deadSprite.visible = false;
            this.damaged1Sprite.visible = false;
            this.damaged2Sprite.visible = false;
        }

        if (!this.isDead()) {
            this.parseInput();
        }
    }
    parseInput() {
        var x = 0;
        var y = 0;
        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.W)) {
            y -= 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.S)) {
            y += 1;
        }
        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            x -= 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
            x += 1;
        }
        this.moveTo(x, y);

        var up = this.level.game.input.keyboard.isDown(Phaser.Keyboard.UP);
        var down = this.level.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        var left = this.level.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        var right = this.level.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)

        x = 0;
        y = 0;
        if (up) {
            y -= 1;
        }
        if (down) {
            y += 1;
        }
        if (left) {
            x -= 1;
        } if (right) {
            x += 1;
        }
        if ((x != 0 || y != 0) && !this.busy) {
            this.attack(x, y);
        }
    }

    attack(x, y) {
        if (this.attackPhase == -1) {
            this.busy = true;
            this.attackPhase = 0;
            var coords = this.level.grid.getGridCoords(x + this.gridX, y + this.gridY)
            this.attackWarpupSprite.centerX = coords[0];
            this.attackWarpupSprite.centerY = coords[1];
            this.attackWarpupSprite.visible = true;
            this.attackWarpupSprite.bringToTop();

            this.level.game.time.events.add(Phaser.Timer.SECOND / 2, this.attack, this);
        } else if (this.attackPhase == 0) {
            this.attackPhase = 1;

            this.attackWarpupSprite.visible = false;
            this.attackSprite.centerX = this.attackWarpupSprite.centerX;
            this.attackSprite.centerY = this.attackWarpupSprite.centerY;
            this.attackSprite.visible = true;
            this.attackSprite.bringToTop();
            var coords = this.level.grid.toGridCoords(this.attackSprite);
            this.level.grid.dealDamage(coords[0], coords[1], this.damage);
            this.level.game.time.events.add(Phaser.Timer.SECOND / 4, this.attack, this);
        } else if (this.attackPhase == 1) {
            this.attackPhase = -1;
            this.attackSprite.visible = false;
            this.busy = false;
        }
        // this.attackSprite = this.game.add.sprite(0, 0, "player_attack_base");
    }
}
class ChessPiece extends GridObject {

    constructor(type, damage) {
        super();
        this.type = type;
        this.damage = damage;
        this.phase = -1;
        this.attackSprites = [];
        this.attackModifiers = new Array();
        this.moveArrayIsDouble = false;
    }

    createAttackSprites(num) {

        this.attackSprites = new Array();
        for (var i = 0; i < num; i++) {
            var sprite = this.level.game.add.sprite(0, 0, "attack_marker_base");
            sprite.visible = false;
            this.attackSprites.push(sprite)
        }
        this.attackFinishSprites = new Array();
        for (var i = 0; i < num; i++) {
            var sprite = this.level.game.add.sprite(0, 0, "enemy_attack_finish");
            sprite.visible = false;
            this.attackFinishSprites.push(sprite)
        }
    }
    positionAttackSprites(positions) {
        for (var i = 0; i < positions.length; i++) {
            var current = positions[i];
            var pixelCoords = this.level.grid.getGridCoords(current[0], current[1]);
            this.attackSprites[i].centerX = pixelCoords[0];
            this.attackSprites[i].centerY = pixelCoords[1];
            this.attackSprites[i].visible = true;
            this.attackSprites[i].bringToTop();

            this.attackFinishSprites[i].centerX = pixelCoords[0];
            this.attackFinishSprites[i].centerY = pixelCoords[1];
            this.attackFinishSprites[i].visible = false;
            this.attackFinishSprites[i].bringToTop();
        }
    }
    displayAfterEffect() {
        for (var i = 0; i < this.attackSprites.length; i++) {
            if (this.attackSprites[i].visible == true) {
                this.attackSprites[i].visible = false;
                this.attackFinishSprites[i].visible = true;
            }
        }
    }
    resetSprites() {
        for (var i = 0; i < this.attackSprites.length; i++) {
            this.attackSprites[i].visible = false;
            this.attackFinishSprites[i].visible = false;
        }
    }
    dealDamage(positions) {

        for (var i = 0; i < positions.length; i++) {
            var current = positions[i];
            this.level.grid.dealDamage(current[0], current[1], this.damage);
        }
    }
    generateAttackPositions(attackPositions) {
        var output = new Array();
        for (var i = 0; i < attackPositions.length; i++) {
            var current = attackPositions[i];

            if (this.moveArrayIsDouble) {
                for (var j = 0; j < current.length; j++) {
                    var secondCurrent = current[j];
                    var newOutput = [this.gridX + secondCurrent[0], this.gridY + secondCurrent[1]];
                    if (Math.abs(newOutput[0]) <= 1 && Math.abs(newOutput[1]) <= 1) {
                       console.log(secondCurrent)
                        output.push(newOutput);
                    }
                }
            } else {
                var newOutput = [this.gridX + current[0], this.gridY + current[1]];
                if (Math.abs(newOutput[0]) <= 1 && Math.abs(newOutput[1]) <= 1) {
                    output.push(newOutput);
                }
            }
        }
        return output;
    }
    attack() {
        // console.log(this.phase)
        if (this.type == 1) {
            if (this.isDead()) {
                if (this.phase != -1) {
                    this.busy = false;
                    this.removeFromBoard();
                    this.resetSprites();
                    this.phase = -1;
                    this.sprite.visible = false;
                }
            } else {
                if (this.phase == -1) {
                    this.phase = 0;
                    this.busy = true;
                    //Player pawn activate animation
                    //After animation pawn move to board            
                    this.level.grid.addPiece(this);
                    this.sprite.visible = true;
                    // this.moveTo(x, y);
                    this.level.game.time.events.add(Phaser.Timer.SECOND * this.speed, this.attack, this);
                } else if (this.phase == 0) {
                    this.phase = 1;
                    console.log(this.attackSpaces)
                    this.positionAttackSprites(this.generateAttackPositions(this.attackSpaces));
                    //play the sprites animation
                    this.level.game.time.events.add(Phaser.Timer.SECOND * this.speed, this.attack, this);
                } else if (this.phase == 1) {
                    this.phase = 2;
                    //deal damage
                    //make blue
                    //wait to leave
                    this.dealDamage(this.generateAttackPositions(this.attackSpaces))
                    this.displayAfterEffect();
                    this.level.game.time.events.add((Phaser.Timer.SECOND * this.speed) * .4, this.attack, this);
                } else if (this.phase == 2) {
                    this.busy = false;
                    this.resetSprites();
                    this.phase = 3;
                    this.level.game.time.events.add((Phaser.Timer.SECOND * this.speed), this.attack, this);
                    //leave
                } else if (this.phase == 3) {
                    this.busy = false;
                    this.resetSprites();
                    this.removeFromBoard();
                    this.level.grid.removePiece(this);
                    this.phase = -1;
                    this.sprite.visible = false;
                    //leave
                }
            }
        }
    }
}
class Knight extends ChessPiece {
    constructor(type, damage) {
        super(type, damage);
        this.health = 3;
        this.speed = .5;
        this.moveArrayIsDouble = true;
    }
    create() {
        this.createAttackSprites(4);
    }
}
class Pawn extends ChessPiece {
    constructor(type, damage) {
        super(type, damage);
        this.health = 1;
        this.speed = .5;
    }
    create() {
        this.createAttackSprites(2);
    }
    tutorialAttack() {
        if (this.type == 0) {
            if (this.isDead()) {
                if (this.phase != -1) {
                    this.busy = false;
                    this.removeFromBoard();
                    this.resetSprites();
                    this.phase = -1;
                    this.sprite.visible = false;
                }
            } else {
                if (this.phase == -1) {
                    this.phase = 0;
                    this.busy = true;
                    //Player pawn activate animation
                    //After animation pawn move to board            
                    this.level.grid.addPiece(this);
                    this.sprite.visible = true;
                    this.moveTo(-2, -1)
                    this.level.game.time.events.add(Phaser.Timer.SECOND / 2, this.tutorialAttack, this);
                } else if (this.phase == 0) {
                    this.phase = 1;
                    this.positionAttackSprites(this.generateAttackPositions());
                    //play the sprites animation
                    this.level.game.time.events.add(Phaser.Timer.SECOND / 2, this.tutorialAttack, this);
                } else if (this.phase == 1) {
                    this.phase = 2;
                    //deal damage
                    //make blue
                    //wait to leave
                    this.dealDamage()
                    this.resetSprites();
                    this.level.game.time.events.add(Phaser.Timer.SECOND / 2, this.tutorialAttack, this);
                } else if (this.phase == 2) {
                    this.busy = false;
                    this.removeFromBoard();
                    this.level.grid.removePiece(this);
                    this.phase = -1;
                    this.sprite.visible = false;
                    //leave
                }
            }
        }
    }
}
class Boss {

    constructor() {
        this.phase = 0;
        this.nextDetected = false;
        this.pawns = new Array();
        this.knights = new Array();
        this.bishops = new Array();
        this.rooks = new Array();
        this.outerRingTable = [
            [-1, -2], [0, -2], [1, -2],
            [-2, -1], [2, -1],
            [-2, 0], [2, 0],
            [-2, 1], [2, 1],
            [-1, 2], [0, 2], [1, 2],
        ];
        this.knightPositions = [
            [0, -2],
            [-2, 0], [2, 0],
            [0, 2]
        ]
        this.outerRingPawnAttackDirection = [
            [2], [2], [2],
            [1], [3],
            [1], [3],
            [1], [3],
            [0], [0], [0]
        ];
        this.knightAttackSpaces = [
            [[[0, -1], [0, -2], [-1, -2]], [[0, -1], [0, -2], [1, -2]]],
            [[[1, 0], [2, 0], [2, -1]], [[1, 0], [2, 0], [2, 1]]],
            [[[0, 1], [0, 2], [-1, 2]], [[0, 1], [0, 2], [1, 2]]],
            [[[-1, 0], [-2, 0], [-2, -1]], [[-1, 0], [-2, 0], [-2, 1]]]
        ];
        //up 0, right 1, down 2, left3
        this.pawnDiagonalAttackSpaces = [
            [[-1, -1], [1, -1]],
            [[1, -1], [1, 1]],
            [[-1, 1], [1, 1]],
            [[-1, -1], [-1, 1]]
        ];
        this.pawnStrightAttackSpaces = [
            [[0, -1], [0, -2]],
            [[1, 0], [2, 0]],
            [[0, 1], [0, 2]],
            [[-1, 0], [-2, 0]]
        ];
    }
    create() {

    }
    createPawns(numPawns) {
        this.pawnWidth = null;

        for (var i = 0; i < numPawns; i++) {
            var pawn = new Pawn(1, 1);
            pawn.sprite = this.level.game.add.sprite(0, 0, "pawn_base");
            pawn.level = this.level;
            pawn.create();
            this.pawns.push(pawn);
            if (this.pawnWidth == null) {
                this.pawnWidth = pawn.sprite.width;
            }
        }
        var start = this.sprite.centerX - (((this.pawnWidth) * (this.pawns.length / 2)));
        for (var i = 0; i < this.pawns.length; i++) {
            var pawn = this.pawns[i];
            pawn.sprite.centerX = start + (this.pawnWidth * i);
            pawn.sprite.centerY = this.sprite.bottom + pawn.sprite.height;
        }
    }
    createKnights(num) {
        this.knightWidth = null;

        for (var i = 0; i < num; i++) {
            var knight = new Knight(1, 1);
            knight.sprite = this.level.game.add.sprite(0, 0, "knight_base");
            knight.level = this.level;
            knight.create();
            this.knights.push(knight);
            if (this.knightWidth == null) {
                this.knightWidth = knight.sprite.width;
            }
        }

        for (var i = 0; i < 2; i++) {
            var knight = this.knights[this.knights.length - (i + 1)];
            if (Math.pow(-1, i) == 1) {
                knight.sprite.left = this.sprite.right + knight.sprite.width / 2;
            } else {
                knight.sprite.right = this.sprite.left - knight.sprite.width / 2;
            }
            knight.sprite.top = this.sprite.centerY;
        }
    }
    update() {

    }
    pawnAttack(speed, pawnOffset, type) {

        //Phase 1 has two pawns
        //First decide if attackign vertically horizontally, or split
        // var axis = Math.floor(Math.random() * 3);
        //Then decide which stragety 
        //Stragty 1 is a straight attack pawn and a diagonal pawn next to each other
        //Stragety two diagaonal pawns next to each other.
        //Stragety two staight pawns at completley random locations.


        var stragety = Math.floor(Math.random() * 3);

        if (type == 0) {
            var pawn0 = this.pawns[pawnOffset + 0];
            var pawn1 = this.pawns[pawnOffset + 1];
            pawn0.speed = speed;
            pawn1.speed = speed;
            if (stragety == 0) {
                var index0 = Math.floor(Math.random() * 12);
                var index1 = Math.abs((index0 - 1) % 12);
                var pos0 = this.outerRingTable[index0];
                var pos1 = this.outerRingTable[index1];

                pawn0.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index0]];
                pawn0.moveTo(pos0[0], pos0[1])
                pawn1.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index1]];
                pawn1.moveTo(pos1[0], pos1[1]);
                pawn0.attack();
                pawn1.attack();
            }
            if (stragety == 1) {
                var index0 = Math.floor(Math.random() * 12);
                var index1 = Math.abs((index0 - 1) % 12);
                var pos0 = this.outerRingTable[index0];
                var pos1 = this.outerRingTable[index1];

                pawn0.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index0]];
                pawn0.moveTo(pos0[0], pos0[1])
                pawn1.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index1]];
                pawn1.moveTo(pos1[0], pos1[1]);
                pawn0.attack();
                pawn1.attack();
            }
            if (stragety == 2) {
                var index0 = Math.floor(Math.random() * 12);
                var index1 = Math.floor(Math.random() * 12);
                while (index0 == index1) {
                    index1 = Math.floor(Math.random() * 12);
                }
                var pos0 = this.outerRingTable[index0];
                var pos1 = this.outerRingTable[index1];

                pawn0.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index0]];
                pawn0.moveTo(pos0[0], pos0[1])
                pawn1.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index1]];
                pawn1.moveTo(pos1[0], pos1[1]);
                pawn0.attack();
                pawn1.attack();
            }
        } else if (type == 1) {
            var pawn0 = this.pawns[pawnOffset + 0];
            var pawn1 = this.pawns[pawnOffset + 1];
            var pawn2 = this.pawns[pawnOffset + 2];
            pawn0.speed = speed;
            pawn1.speed = speed;
            pawn2.speed = speed;
            if (stragety == 0) {
                var index0 = Math.floor(Math.random() * 12);
                var index1 = Math.abs((index0 - 2) % 12);
                var index2 = Math.abs((index0 + 2) % 12);
                var pos0 = this.outerRingTable[index0];
                var pos1 = this.outerRingTable[index1];
                var pos2 = this.outerRingTable[index2];

                pawn0.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index0]];
                pawn0.moveTo(pos0[0], pos0[1])
                pawn0.attack();

                pawn1.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index1]];
                pawn1.moveTo(pos1[0], pos1[1]);
                pawn1.attack();

                pawn2.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index2]];
                pawn2.moveTo(pos2[0], pos2[1]);
                pawn2.attack();
            }
            if (stragety == 1) {
                var index0 = Math.floor(Math.random() * 12);
                var index1 = Math.abs((index0 - 2) % 12);
                var index2 = Math.abs((index0 + 2) % 12);
                var pos0 = this.outerRingTable[index0];
                var pos1 = this.outerRingTable[index1];
                var pos2 = this.outerRingTable[index2];

                pawn0.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index0]];
                pawn0.moveTo(pos0[0], pos0[1])
                pawn0.attack();

                pawn1.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index1]];
                pawn1.moveTo(pos1[0], pos1[1]);
                pawn1.attack();

                pawn2.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index2]];
                pawn2.moveTo(pos2[0], pos2[1]);
                pawn2.attack();
            }
            if (stragety == 2) {
                var index0 = Math.floor(Math.random() * 12);
                var index1 = Math.floor(Math.random() * 12);
                var index2 = Math.floor(Math.random() * 12);
                while (index0 == index1 || index1 == index2) {
                    index1 = Math.floor(Math.random() * 12);
                }
                while (index0 == index2 || index1 == index2) {
                    index2 = Math.floor(Math.random() * 12);
                }
                var pos0 = this.outerRingTable[index0];
                var pos1 = this.outerRingTable[index1];
                var pos2 = this.outerRingTable[index2];

                pawn0.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index0]];
                pawn0.moveTo(pos0[0], pos0[1])
                pawn0.attack();

                pawn1.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index1]];
                pawn1.moveTo(pos1[0], pos1[1]);
                pawn1.attack();

                pawn2.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index2]];
                pawn2.moveTo(pos2[0], pos2[1]);
                pawn2.attack();
            }
        } else if (type == 2) {
            var pawn0 = this.pawns[pawnOffset + 0];
            var pawn1 = this.pawns[pawnOffset + 1];
            var pawn2 = this.pawns[pawnOffset + 2];
            pawn0.speed = speed;
            pawn1.speed = speed;
            pawn2.speed = speed;
            if (stragety == 0) {
                var index0 = Math.floor(Math.random() * 12);
                var index1 = Math.abs((index0 - 1) % 12);
                var index2 = Math.abs((index0 + 1) % 12);
                var pos0 = this.outerRingTable[index0];
                var pos1 = this.outerRingTable[index1];
                var pos2 = this.outerRingTable[index2];

                pawn0.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index0]];
                pawn0.moveTo(pos0[0], pos0[1])
                pawn0.attack();

                pawn1.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index1]];
                pawn1.moveTo(pos1[0], pos1[1]);
                pawn1.attack();

                pawn2.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index2]];
                pawn2.moveTo(pos2[0], pos2[1]);
                pawn2.attack();
            }
            if (stragety == 1) {
                var index0 = Math.floor(Math.random() * 12);
                var index1 = Math.abs((index0 - 1) % 12);
                var index2 = Math.abs((index0 + 1) % 12);
                var pos0 = this.outerRingTable[index0];
                var pos1 = this.outerRingTable[index1];
                var pos2 = this.outerRingTable[index2];

                pawn0.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index0]];
                pawn0.moveTo(pos0[0], pos0[1])
                pawn0.attack();

                pawn1.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index1]];
                pawn1.moveTo(pos1[0], pos1[1]);
                pawn1.attack();

                pawn2.attackSpaces = this.pawnDiagonalAttackSpaces[this.outerRingPawnAttackDirection[index2]];
                pawn2.moveTo(pos2[0], pos2[1]);
                pawn2.attack();
            }
            if (stragety == 2) {
                var index0 = Math.floor(Math.random() * 12);
                var index1 = Math.floor(Math.random() * 12);
                var index2 = Math.floor(Math.random() * 12);
                while (index0 == index1 || index1 == index2) {
                    index1 = Math.floor(Math.random() * 12);
                }
                while (index0 == index2 || index1 == index2) {
                    index2 = Math.floor(Math.random() * 12);
                }
                var pos0 = this.outerRingTable[index0];
                var pos1 = this.outerRingTable[index1];
                var pos2 = this.outerRingTable[index2];

                pawn0.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index0]];
                pawn0.moveTo(pos0[0], pos0[1])
                pawn0.attack();

                pawn1.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index1]];
                pawn1.moveTo(pos1[0], pos1[1]);
                pawn1.attack();

                pawn2.attackSpaces = this.pawnStrightAttackSpaces[this.outerRingPawnAttackDirection[index2]];
                pawn2.moveTo(pos2[0], pos2[1]);
                pawn2.attack();
            }
        }
    }
    knightAttack(speed, pawnOffset, type) {

        //Phase 1 has two pawns
        //First decide if attackign vertically horizontally, or split
        // var axis = Math.floor(Math.random() * 3);
        //Then decide which stragety 
        //Stragty 1 is a straight attack pawn and a diagonal pawn next to each other
        //Stragety two diagaonal pawns next to each other.
        //Stragety two staight pawns at completley random locations.


        var stragety = Math.floor(Math.random() * 2);

        if (type == 0) {
            var knight0 = this.knights[pawnOffset + 0];
            var knight1 = this.knights[pawnOffset + 1];
            knight0.speed = speed;
            knight1.speed = speed;
            var index0;
            var index1;
            var pos0;
            var pos1;
            if (stragety == 0) {
                index0 = Math.floor(Math.random() * 4);
                index1 = Math.abs((index0 - 1) % 4);
                pos0 = this.knightPositions[index0];
                pos1 = this.knightPositions[index1];
            } else if (stragety == 1) {
                index0 = Math.floor(Math.random() * 4);
                index1 = Math.abs((index0 - 2) % 4);
                pos0 = this.knightPositions[index0];
                pos1 = this.knightPositions[index1];
            }
            knight0.attackSpaces = this.knightAttackSpaces[index0];
            knight0.moveTo(pos0[0], pos0[1])

            knight1.attackSpaces  = this.knightAttackSpaces[index1];
            knight1.moveTo(pos1[0], pos1[1]);

            knight0.attack();
            knight1.attack();
        }
    }
    getEmptyPawnPosition() {
        var index0 = Math.floor(Math.random() * 12);
        var pos0 = this.outerRingTable[index0];

    }
}
class TutorialBoss extends Boss {

    constructor() {
        super();
        this.advancePhase = true;
    }
    create() {
        this.dialog = ["I am the black king, the original ruler of this land.\nYour task is to become my successor by conquring\nthe challenges laid before you.\nGood Luck",
            "Your two most important abilities are\nMovement: WASD and\nAttacking: Arrow Keys\nYou have a set of bombs at your disposal\nthey will explode after a short delay so dont walk over them",
            "You can move within the lighter squares\nwhile your enemies may move within the darker squares",
            "When an enemy is about to attack the\nthreatened squares will have flashing\nblue boxes on them.\nKill the pawn the press [SPACE] to continue.",

            "Pawns take 1 hit to kill as they are very weak.\nBishops take 3 hits\nRooks take 4 hits.",
            "Should you fail to dodge an attack you will take\ndamage, you have 3 chances, be careful lest\nyou lose them all",
            "Finally a piece will have a blue outline when it\nis vulnerable and a grey outline when it is\ninvulnerable use this to your advantage to\navoid putting yourself in danger.",
            "You are now ready to earn the title of king."
        ]
        var style = { font: "40px Calibri", fill: "#000000", align: "center" };
        this.text = this.level.game.add.text(0, 0, this.dialog[this.phase], style);
        this.text.centerX = this.sprite.centerX;
        this.text.centerY = this.sprite.bottom + this.text.height / 2;

        var spaceBarText = this.level.game.add.text(0, 0, "Press [SPACE] to advance", style);
        spaceBarText.centerX = this.sprite.centerX;
        spaceBarText.y = 0 + 5;

        this.pawn = new Pawn(0, 1);
        this.pawn.level = this.level;
        this.pawn.idleSprite = this.level.game.add.sprite(0, 0, "pawn_base");
        this.pawn.idleSprite.centerX = this.sprite.right + this.pawn.idleSprite.width;
        this.pawn.idleSprite.centerY = this.sprite.centerY;
        this.pawn.idleSprite.visible = false;
        this.pawn.sprite = this.level.game.add.sprite(0, 0, "pawn_grid");
        this.pawn.sprite.visible = false;
        this.pawn.createAttackSprites(2);

    }

    update() {
        var down = this.level.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR);
        if (down) {
            if (!this.nextDetected) {
                this.showNextText();
                this.nextDetected = true;
            }
        } else {
            this.nextDetected = false;
        }
    }

    showNextText() {
        if (this.phase >= this.dialog.length - 1) {
            this.level.game.state.start('levelSelect');
        } else {
            if (this.phase == 3) {
                if (this.advancePhase == true) {
                    // this.pawn.sprite.visible = true;                    
                    this.pawn.idleSprite.visible = true;
                    this.tutorialAttack();
                    this.advancePhase = false;
                } else {
                    if (this.pawn.health == 0) {
                        this.advancePhase = true;
                    }
                }
            }
            if (this.advancePhase) {
                this.phase = this.phase + 1;
                this.text.text = this.dialog[this.phase];
                this.text.centerX = this.sprite.centerX;
                this.text.centerY = this.sprite.bottom + this.text.height / 2;
            }
        }
    }

    tutorialAttack() {
        if (this.phase == 3) {
            this.level.game.time.events.add(Phaser.Timer.SECOND, this.tutorialAttack, this);
            if (!this.pawn.busy) {
                this.pawn.tutorialAttack();
            }
        }
    }
}

class PawnBoss extends Boss {

    constructor() {
        super();
        this.currentlyAttacking = false;
        this.numPawnsDead = 0;
    }
    create() {
        this.createPawns(8);
        this.createKnights(2);
        this.level.game.time.events.loop(Phaser.Timer.SECOND * 2, this.decide, this);
    }
    update() {
    }
    decide() {
        this.numPawnsDead = 0;
        var numAttacking = 0;
        for (var i = 0; i < this.pawns.length; i++) {
            var pawn = this.pawns[i];
            if (pawn.isDead()) {
                this.numPawnsDead++;
            } else if (pawn.busy) {
                numAttacking++;
            }

        }
        this.currentlyAttacking = numAttacking > 0;

        if (this.numPawnsDead < 2) {
            this.phase = 0;
            if (!this.currentlyAttacking) {

                this.knightAttack(1, 0, 0);
                // this.pawnAttack(1, 0, 0);
            }
        } else if (this.numPawnsDead < 5) {

            this.phase = 1;
            this.pawnAttack(1, 2, 1);
        } else if (this.numPawnsDead < 8) {

            this.phase = 2;
            this.pawnAttack(1, 5, 2);
            this.knightAttack(1, 5, 0);
        }
    }
}

class King extends Boss {

    create() {

        this.pawns = new Array();
        this.bishops = new Array();
        this.pawnWidth = null;

        for (var i = 0; i < 8; i++) {
            var pawn = new ChessPiece();
            pawn.sprite = this.level.game.add.sprite(0, 0, "pawn_base");
            this.pawns.push(pawn);
            if (this.pawnWidth == null) {
                this.pawnWidth = pawn.sprite.width;
            }
        }
    }
    update() {
        this.updatePositions();
    }
    updatePositions() {
        var start = this.sprite.centerX - ((this.pawnWidth * this.pawns.length) / 2);
        for (var i = 0; i < this.pawns.length; i++) {
            var pawn = this.pawns[i];
            pawn.sprite.centerX = start + (this.pawnWidth * i);
            pawn.sprite.centerY = this.sprite.bottom + pawn.sprite.height;
        }
    }
}