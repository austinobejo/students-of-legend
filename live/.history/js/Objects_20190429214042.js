class WorldObject {
    constructor(game, level) {
        this.game = game;
        this.level = level;
        this.characterSpeed = 50000;
        this.weapon = null;
        this.bulletSpeed = 70000;
        this.timer = false;
        this.shotWait = 1;
        this.health = 100;
        this.isDead = false;
    };
    fireBullet(startPos, velocity, source) {

        var bulletObject = this.level.bulletStockpile.shift();
        var bullet = bulletObject.sprite;

        bullet.centerX = startPos[0];
        bullet.centerY = startPos[1];
        bullet.body.velocity.x = velocity[0];
        bullet.body.velocity.y = velocity[1];
        bullet.visible = true;
        this.level.bulletStockpile.push(bulletObject);
        bulletObject.sound.play();
        bulletObject.source = source;
    };
    create() {

        this.centerXOffset = this.level.map.tileWidth / 2;
        this.centerYOffset = this.level.map.tileHeight / 2;
    }
    update() {
        if (this.timer == false) {
            this.game.time.events.loop(Phaser.Timer.SECOND * this.shotWait, this.shoot, this);
            this.timer = true;
        }

        if (this.health <= 0) {
            this.onDeath();
        }
    };
    getDirection(source, destination) {

        var x = destination.centerX - source.centerX;
        var y = source.centerY - destination.centerY;
        var sqrt = Math.sqrt((x * x) + (y * y));
        x = x / sqrt;
        y = y / sqrt;
        return [x, y]
    }

    shoot() {

    }

    onDeath() {

    }
}

class Player extends WorldObject {
    constructor(game, level) {
        super(game, level);

        this.shotWait = .1;
    }
    preload() {

        this.game.load.image('player', 'resources/players/player.png');
    }
    create() {
        super.create();
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.sprite = this.game.add.sprite(0, 0, "player");
        this.game.physics.arcade.enable(this.sprite);
    }
    parseInput() {
        this.sprite.body.velocity.x = 0;
        this.sprite.body.velocity.y = 0;

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.W)) {

            this.sprite.body.velocity.y -= this.characterSpeed * this.game.time.physicsElapsed;
        } if (this.game.input.keyboard.isDown(Phaser.Keyboard.S)) {

            this.sprite.body.velocity.y += this.characterSpeed * this.game.time.physicsElapsed;
        }

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {

            this.sprite.body.velocity.x -= this.characterSpeed * this.game.time.physicsElapsed;
        } if (this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {

            this.sprite.body.velocity.x += this.characterSpeed * this.game.time.physicsElapsed;
        }

        this.up = this.game.input.keyboard.isDown(Phaser.Keyboard.UP);
        this.down = this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        this.left = this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        this.right = this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
    }
    shoot() {
        console.log(this.health);
        var source = "player"
        if (this.up && this.right) {
            this.fireBullet([this.sprite.x + this.centerXOffset * 1.5, this.sprite.y - this.centerYOffset],
                [(this.bulletSpeed * this.game.time.physicsElapsed) / 2, -(this.bulletSpeed * this.game.time.physicsElapsed) / 2],
                source);
        } else if (this.up && this.left) {
            this.fireBullet([this.sprite.x + -this.centerXOffset * 1.5, this.sprite.y - this.centerYOffset * 1.5],
                [-(this.bulletSpeed * this.game.time.physicsElapsed) / 2, -(this.bulletSpeed * this.game.time.physicsElapsed) / 2],
                source);
        } else if (this.down && this.left) {
            this.fireBullet([this.sprite.x + -this.centerXOffset * 1.5, this.sprite.y + this.centerYOffset * 1.5],
                [-(this.bulletSpeed * this.game.time.physicsElapsed) / 2, (this.bulletSpeed * this.game.time.physicsElapsed) / 2],
                source);
        } else if (this.down && this.right) {
            this.fireBullet([this.sprite.x + this.centerXOffset * 1.5, this.sprite.y + this.centerYOffset * 1.5],
                [(this.bulletSpeed * this.game.time.physicsElapsed) / 2, (this.bulletSpeed * this.game.time.physicsElapsed) / 2],
                source);
        } else if (this.up) {
            this.fireBullet([this.sprite.x + this.centerXOffset, this.sprite.y + this.centerYOffset],
                [0, -this.bulletSpeed * this.game.time.physicsElapsed],
                source);
        } else if (this.down) {
            this.fireBullet([this.sprite.x + 0, this.sprite.y + this.centerYOffset * 2],
                [0, this.bulletSpeed * this.game.time.physicsElapsed],
                source);
        } else if (this.left) {
            this.fireBullet([this.sprite.x + -this.centerXOffset * 2, this.sprite.y + 0],
                [-this.bulletSpeed * this.game.time.physicsElapsed, 0],
                source);
        } else if (this.right) {
            this.fireBullet([this.sprite.x + this.centerXOffset * 2, this.sprite.y + this.centerYOffset / 4],
                [this.bulletSpeed * this.game.time.physicsElapsed, 0],
                source);
        }
    }
    onDeath() {
        this.isDead = true;
    }
}

class Enemy extends WorldObject {


}

class StraightTurrent extends Enemy {
    constructor(game, level) {
        super(game, level);

        this.shotWait = .5;
    }
    shoot() {
        var direction = this.getDirection(this.sprite, this.level.player.sprite);

        for (var i = 0; i < 5; i++) {

            this.fireBullet([this.sprite.centerX + direction[0] * 100 * i, this.sprite.centerY - direction[1] * 100 * i],
                [direction[0] * 1000, -direction[1] * 1000],
                "enemy");
        }
    }
}

class HorizontalTurrent extends Enemy {
    constructor(game, level) {
        super(game, level);

        this.shotWait = .01;
    }
    shoot() {
        var direction = this.getDirection(this.sprite, this.level.player.sprite);

        this.fireBullet([this.sprite.centerX + direction[0] * 100, this.sprite.centerY - direction[1] * 100],
            [direction[0] * 1000, -direction[1] * 1000],
            "enemy");
    }
}

class Weapon {

    constructor(sprite) {
        this.sprite = sprite
    }
    shoot(direction) {

    };
}

class Bullet {
    constructor(position, color) {
        this.position = position;
        this.color = color;
        this.damage = 10;
    }
}