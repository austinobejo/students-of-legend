class Grid {
    constructor() {
        this.grid = new Array();
    }
    addPiece(piece) {
        if (piece.onBoard) {
            piece.sprite.centerX = this.sprite.centerX + (this.cellWidth * piece.gridX);
            piece.sprite.centerY = this.sprite.centerY + (this.cellHeight * piece.gridY);
        }
        this.grid.push(piece);
    }
    removePiece(piece) {
        var foundIndex = null;

        for (var i = 0; i < this.grid.length && foundIndex == null; i++) {

            if (this.grid[i] == piece) {
                foundIndex = i
            }
        }
        if (foundIndex != null) {
            this.grid.splice(foundIndex);
        }
    }
    setPlayer(player) {
        this.player = player;
        this.addPiece(player);
    }
    create() {
        this.cellWidth = this.sprite.width / 5.0;
        this.cellHeight = this.sprite.height / 5.0;
    }
    update() {
        var deadThings = new Array();
        for (var i = 0; i < this.grid.length; i++) {
            var current = this.grid[i];
            if (current.isDead()) {
                deadThings.push(current)
            } else if (current.onBoard) {
                current.sprite.centerX = this.sprite.centerX + (this.cellWidth * current.gridX);
                current.sprite.centerY = this.sprite.centerY + (this.cellHeight * current.gridY);
                current.sprite.bringToTop();
            }
        }

        for (var i = 0; i < deadThings.length; i++) {
            this.removePiece(deadThings[i]);
        }
    }
    getGridCoords(x, y) {

        return [this.sprite.centerX + (this.cellWidth * x), this.sprite.centerY + (this.cellHeight * y)]
    }
}

class GridObject {
    constructor() {
        this.gridX = 0;
        this.gridY = 0;
        this.onBoard = false;
        this.damage = 0;
        this.health = 10;
        this.busy = false;
    }

    isDead() {
        return this.health <= 0;
    }
    moveTo(x, y) {
        this.gridX = x;
        this.gridY = y;
        this.onBoard = true;
    }
    removeFromBoard() {
        this.onBoard = false;
    }
}

class Player extends GridObject {

    constructor() {
        super();
        this.moveTo(0, 0);
    }
    update() {
        this.parseInput();
    }
    parseInput() {
        var x = 0;
        var y = 0;
        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.W)) {
            y -= 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.S)) {
            y += 1;
        }
        if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            x -= 1;
        } if (this.level.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
            x += 1;
        }
        this.moveTo(x, y);
        // this.up = this.level.game.input.keyboard.isDown(Phaser.Keyboard.UP);
        // this.down = this.level.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        // this.left = this.level.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        // this.right = this.level.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
    }
}


class ChessPiece extends GridObject {

    constructor(type, damage) {
        super();
        this.type = type;
        this.damage = damage;
        this.phase = -1;
        this.attackSprites = [];
    }

    createAttackSprites(num) {

        this.attackSprites = new Array();
        for (var i = 0; i < num; i++) {
            var sprite = this.level.game.add.sprite(0, 0, "attack_marker_base");
            sprite.visible = false;
            this.attackSprites.push(sprite)
        }
    }
    positionAttackSprites(positions) {

        for (var i = 0; i < positions.length; i++) {
            var current = positions[i];
            var pixelCoords = this.level.grid.getGridCoords(current[0], current[1]);
            this.attackSprites[i].x = (this.sprite.x + (this.sprite.width * pixelCoords[0]));
            this.attackSprites[i].y = (this.sprite.y + (this.sprite.height * pixelCoords[1]));
            console.log((this.sprite.x + (this.sprite.width * pixelCoords[0])))
            console.log((this.sprite.y + (this.sprite.height * pixelCoords[1])))
            console.log("---")
            this.attackSprites[i].visible = true;
            this.attackSprites[i].bringToTop();
        }
    }
    resetSprites() {
        for (var i = 0; i < this.attackSprites.length; i++) {
            this.attackSprites[i].visible = false;
        }
    }
}

class Pawn extends ChessPiece {
    constructor(type, damage) {
        super(type, damage);
        this.health = 1;
    }
    create() {
        // this.attacks = new Array();
        // var timer = this.level.game.time.create(false);
        // timer.loop(2000, attack1, this);
    }
    attack() {
        console.log(this.phase)
        if (this.phase == -1) {
            this.phase = 0;
            this.busy = true;
            //Player pawn activate animation
            //After animation pawn move to board            
            this.level.grid.addPiece(this);
            this.sprite.visible = true;
            this.moveTo(-2, -1)
            this.level.game.time.events.add(Phaser.Timer.SECOND / 2, this.attack, this);
        } else if (this.phase == 0) {
            this.phase = 1;
            this.positionAttackSprites(this.generateAttackPositions());
            //play the sprites animation
            this.level.game.time.events.add(Phaser.Timer.SECOND / 2, this.attack, this);
        } else if (this.phase == 1) {
            this.phase = 2;
            //deal damage
            //make blue
            //wait to leave
            this.resetSprites();
            this.level.game.time.events.add(Phaser.Timer.SECOND / 2, this.attack, this);
        } else if (this.phase == 2) {
            this.busy = false;
            this.removeFromBoard();
            this.level.grid.removePiece(this);
            this.phase = -1;
            this.sprite.visible = false;
            //leave
        }
    }
    generateAttackPositions() {
        var modifiers = [[-1, 1], [1, 1]];
        var output = new Array();
        for (var i = 0; i < modifiers.length; i++) {
            var current = modifiers[i];
            var newOutput = [this.sprite.gridX + current[0], this.sprite.gridY + current[1]]
            if (!(Math.abs(newOutput[0]) > 2 || Math.abs(newOutput[1]) > 2)) {
                output.push(newOutput);
            }
        }
        return output;
    }
}
class Boss {

    constructor() {
        this.phase = 0;
        this.nextDetected = false;
    }
    create() {

    }
    update() {

    }
}

class TutorialBoss extends Boss {

    constructor() {
        super();
        this.advancePhase = true;
    }
    create() {
        this.dialog = ["I am the black king, the original ruler of this land.\nYour task is to become my successor by conquring\nthe challenges laid before you.\nGood Luck",
            "Your two most important abilities are\nMoving by using WASD and\nAttacking by using the Arrow Keys",
            "You can move within the lighter squares\nwhile your enemies may move within the darker squares",
            "When an enemy is about to attack the\nthreatened squares will have flashing\nblue boxes on them.\nKill the pawn to continue.",
            
            "You will learn the way each piece attack in time",
            "Should you fail to dodge an attack you will take\ndamage, you have 3 chances, be careful lest\nyou lose them all",
            "Finally a piece will have a blue outline when it\nis vulnerable and a grey outline when it is\ninvulnerable use this to your advantage to\navoid putting yourself in danger.",
            "You are now ready to earn the title of king."
        ]
        var style = { font: "40px Calibri", fill: "#000000", align: "center" };
        this.text = this.level.game.add.text(0, 0, this.dialog[this.phase], style);
        this.text.centerX = this.sprite.centerX;
        this.text.centerY = this.sprite.bottom + this.text.height / 2;

        var spaceBarText = this.level.game.add.text(0, 0, "Press [SPACE] to advance", style);
        spaceBarText.centerX = this.sprite.centerX;
        spaceBarText.y = 0 + 5;

        this.pawn = new Pawn(0, 0);
        this.pawn.level = this.level;
        this.pawn.idleSprite = this.level.game.add.sprite(0, 0, "pawn_base");
        this.pawn.idleSprite.centerX = this.sprite.right + this.pawn.idleSprite.width;
        this.pawn.idleSprite.centerY = this.sprite.centerY;
        this.pawn.idleSprite.visible = false;
        this.pawn.sprite = this.level.game.add.sprite(0, 0, "pawn_grid");
        this.pawn.sprite.visible = false;
        this.pawn.createAttackSprites(2);

    }

    update() {
        var down = this.level.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR);
        if (down) {
            if (!this.nextDetected) {
                this.showNextText();
                this.nextDetected = true;
            }
        } else {
            this.nextDetected = false;
        }
    }

    showNextText() {

        if (this.phase >= this.dialog.length - 1) {
            this.level.game.state.start('levelSelect');
        } else {
            if (this.phase == 3) {
                if (this.advancePhase == true) {
                    // this.pawn.sprite.visible = true;                    
                    this.pawn.idleSprite.visible = true;
                    this.tutorialAttack();
                    this.advancePhase = false;
                } else {
                    if (this.pawn.health == 0) {
                        this.advancePhase = true;
                    }
                }
            }

            if (this.advancePhase) {
                this.phase = this.phase + 1;
                this.text.text = this.dialog[this.phase];
                this.text.centerX = this.sprite.centerX;
                this.text.centerY = this.sprite.bottom + this.text.height / 2;
            }
        }
    }

    tutorialAttack() {
        if (this.phase == 3) {
            this.level.game.time.events.add(Phaser.Timer.SECOND, this.tutorialAttack, this);
            if (!this.pawn.busy) {
                this.pawn.attack();
            }
        }
    }
}
class King extends Boss {

    create() {

        this.pawns = new Array();
        this.bishops = new Array();
        this.pawnWidth = null;

        for (var i = 0; i < 8; i++) {
            var pawn = new ChessPiece();
            pawn.sprite = this.level.game.add.sprite(0, 0, "pawn_base");
            this.pawns.push(pawn);
            if (this.pawnWidth == null) {
                this.pawnWidth = pawn.sprite.width;
            }
        }
    }
    update() {
        this.updatePositions();
    }
    updatePositions() {
        var start = this.sprite.centerX - ((this.pawnWidth * this.pawns.length) / 2);
        for (var i = 0; i < this.pawns.length; i++) {
            var pawn = this.pawns[i];
            pawn.sprite.centerX = start + (this.pawnWidth * i);
            pawn.sprite.centerY = this.sprite.bottom + pawn.sprite.height;
        }
    }
}