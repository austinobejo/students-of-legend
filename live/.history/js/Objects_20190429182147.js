class WorldObject{
    constructor(game) {
        this.game = game;
        this.characterSpeed = 50000;
        this.weapon = null;
        this.bulletSpeed = 70000;
    }
    preload();
    create(map);
    move();
    fireBullet(startPos, velocity) {

        var bullet = this.bulletStockpile.shift();

        bullet.x = this.selectedCharacter.sprite.x + this.centerXOffset;
        bullet.y = this.selectedCharacter.sprite.y + this.centerYOffset;
        bullet.body.velocity.x = velocity[0];
        bullet.body.velocity.y = velocity[1];
        bullet.visible = true;
        this.bulletStockpile.push(bullet);
    }
}

class Player extends WorldObject {

    preload() {

    }
    create(map) {

        this.cursors = this.game.input.keyboard.createCursorKeys();
    }
    move() {
        this.sprite.body.velocity.x = 0;
        this.sprite.body.velocity.y = 0;

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.W)) {

            this.sprite.body.velocity.y -= this.characterSpeed * this.game.time.physicsElapsed;
        } if (this.game.input.keyboard.isDown(Phaser.Keyboard.S)) {

            this.sprite.body.velocity.y += this.characterSpeed * this.game.time.physicsElapsed;
        }

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {

            this.sprite.body.velocity.x -= this.characterSpeed * this.game.time.physicsElapsed;
        } if (this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {

            this.sprite.body.velocity.x += this.characterSpeed * this.game.time.physicsElapsed;
        }

        var up = this.game.input.keyboard.isDown(Phaser.Keyboard.UP);
        var down = this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        var left = this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        var right = this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)

        if (up && right) {

            this.fireBullet([this.centerXOffset * 1.5, -this.centerYOffset * 1.5], [(this.bulletSpeed * this.game.time.physicsElapsed) / 2, -(this.bulletSpeed * this.game.time.physicsElapsed) / 2]);
        } else if (up && left) {

            this.fireBullet([-this.centerXOffset * 1.5, -this.centerYOffset * 1.5], [-(this.bulletSpeed * this.game.time.physicsElapsed) / 2, -(this.bulletSpeed * this.game.time.physicsElapsed) / 2]);
        } else if (down && left) {

            this.fireBullet([-this.centerXOffset * 1.5, this.centerYOffset * 1.5], [-(this.bulletSpeed * this.game.time.physicsElapsed) / 2, (this.bulletSpeed * this.game.time.physicsElapsed) / 2]);
        } else if (down && right) {

            this.fireBullet([this.centerXOffset * 1.5, this.centerYOffset * 1.5], [(this.bulletSpeed * this.game.time.physicsElapsed) / 2, (this.bulletSpeed * this.game.time.physicsElapsed) / 2]);
        } else if (up) {

            this.fireBullet([this.centerXOffset, this.centerYOffset], [0, -this.bulletSpeed * this.game.time.physicsElapsed]);
        } else if (down) {

            this.fireBullet([0, this.centerYOffset * 2], [0, this.bulletSpeed * this.game.time.physicsElapsed]);
        } else if (left) {

            this.fireBullet([-this.centerXOffset * 2, 0], [-this.bulletSpeed * this.game.time.physicsElapsed, 0]);
        } else if (right) {

            this.fireBullet([this.centerXOffset * 2, 0], [this.bulletSpeed * this.game.time.physicsElapsed, 0]);
        }
    }
}

class Enemy extends WorldObject{

    
}

class StraightTurrent extends Enemy {

    preload() {

    }
}

class HorizontalTurrent extends Enemy {

    preload() {

    }
}

class Weapon {

    constructor(sprite) {
        this.sprite = sprite
    }
    shoot(direction) {

    };
}

class Bullet {
    constructor(position, color) {
        this.position = position;
        this.color = color;
    }

    onHit() {
        
    }
}