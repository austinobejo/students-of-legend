class WorldObject {
    constructor(game, level) {
        this.game = game;
        this.level = level;
        this.characterSpeed = 50000;
        this.weapon = null;
        this.bulletSpeed = 70000;
    };
    fireBullet(startPos, velocity, source) {

        var bulletObject = this.level.bulletStockpile.shift();
        var bullet = bulletObject.sprite;

        bullet.centerX = startPos[0];
        bullet.centerY = startPos[1];
        bullet.body.velocity.x = velocity[0];
        bullet.body.velocity.y = velocity[1];
        bullet.visible = true;
        this.level.bulletStockpile.push(bulletObject);
        bulletObject.sound.play();
        bulletObject.source = source;
    };
    create() {

        this.centerXOffset = this.level.map.tileWidth / 2;
        this.centerYOffset = this.level.map.tileHeight / 2;
    }
    update() {

    };
    getDirection(source, destination) {

        var x = destination.centerX - source.centerX;
        var y = source.centerY - destination.centerY;
        var sqrt = Math.sqrt((x * x) + (y * y));
        x = x / sqrt;
        y = y / sqrt;
        return [x, y]
    }
}

class Player extends WorldObject {

    preload() {

        this.game.load.image('player', 'resources/players/player.png');
    }
    create() {
        super.create();
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.sprite = this.game.add.sprite(0, 0, "player");
        this.game.physics.arcade.enable(this.sprite);
    }
    parseInput() {
        this.sprite.body.velocity.x = 0;
        this.sprite.body.velocity.y = 0;

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.W)) {

            this.sprite.body.velocity.y -= this.characterSpeed * this.game.time.physicsElapsed;
        } if (this.game.input.keyboard.isDown(Phaser.Keyboard.S)) {

            this.sprite.body.velocity.y += this.characterSpeed * this.game.time.physicsElapsed;
        }

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {

            this.sprite.body.velocity.x -= this.characterSpeed * this.game.time.physicsElapsed;
        } if (this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {

            this.sprite.body.velocity.x += this.characterSpeed * this.game.time.physicsElapsed;
        }

        var up = this.game.input.keyboard.isDown(Phaser.Keyboard.UP);
        var down = this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        var left = this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        var right = this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
        var source = "player"
        if (up && right) {
            this.fireBullet([this.sprite.x + this.centerXOffset * 1.5, this.sprite.y - this.centerYOffset],
                [(this.bulletSpeed * this.game.time.physicsElapsed) / 2, -(this.bulletSpeed * this.game.time.physicsElapsed) / 2],
                source);
        } else if (up && left) {
            this.fireBullet([this.sprite.x + -this.centerXOffset * 1.5, this.sprite.y - this.centerYOffset * 1.5],
                [-(this.bulletSpeed * this.game.time.physicsElapsed) / 2, -(this.bulletSpeed * this.game.time.physicsElapsed) / 2],
                source);
        } else if (down && left) {
            this.fireBullet([this.sprite.x + -this.centerXOffset * 1.5, this.sprite.y + this.centerYOffset * 1.5],
                [-(this.bulletSpeed * this.game.time.physicsElapsed) / 2, (this.bulletSpeed * this.game.time.physicsElapsed) / 2],
                source);
        } else if (down && right) {
            this.fireBullet([this.sprite.x + this.centerXOffset * 1.5, this.sprite.y + this.centerYOffset * 1.5],
                [(this.bulletSpeed * this.game.time.physicsElapsed) / 2, (this.bulletSpeed * this.game.time.physicsElapsed) / 2],
                source);
        } else if (up) {
            this.fireBullet([this.sprite.x + this.centerXOffset, this.sprite.y + this.centerYOffset],
                [0, -this.bulletSpeed * this.game.time.physicsElapsed],
                source);
        } else if (down) {
            this.fireBullet([this.sprite.x + 0, this.sprite.y + this.centerYOffset * 2],
                [0, this.bulletSpeed * this.game.time.physicsElapsed],
                source);
        } else if (left) {
            this.fireBullet([this.sprite.x + -this.centerXOffset * 2, this.sprite.y + 0],
                [-this.bulletSpeed * this.game.time.physicsElapsed, 0],
                source);
        } else if (right) {
            this.fireBullet([this.sprite.x + this.centerXOffset * 2, this.sprite.y + this.centerYOffset / 4],
                [this.bulletSpeed * this.game.time.physicsElapsed, 0],
                source);
        }
    }
}

class Enemy extends WorldObject {


}

class StraightTurrent extends Enemy {

    constructor(game, level) {
        super(game, level);
        this.timer = false;
    }
    update() {

        if(this.timer == false) {
            this.game.time.events.loop(Phaser.Timer.SECOND * 1, this.shoot, this);
            this.timer = true;
        }
      
    }
    shoot() {
        var direction = this.getDirection(this.sprite, this.level.player.sprite);

        this.fireBullet([this.sprite.centerX, this.sprite.centerY],
            [direction[0] * 1000, -direction[1] * 1000],
            "enemy");
    }
}

class HorizontalTurrent extends Enemy {

    preload() {

    }
}

class Weapon {

    constructor(sprite) {
        this.sprite = sprite
    }
    shoot(direction) {

    };
}

class Bullet {
    constructor(position, color) {
        this.position = position;
        this.color = color;
    }

    onOverlap(me, enemy) {
        // console.log(this.sprite);
    }
}