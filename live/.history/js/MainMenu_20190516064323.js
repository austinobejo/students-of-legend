class MenuButton {
	constructor(x, y, key, callback, callbackContext, overFrame,
		outFrame, downFrame, upFrame, text, textStyle, xOffset, yOffset) {
		this.game = callbackContext.game;
		this.buttonImage = this.game.add.button(x, y, key, callback, callbackContext, overFrame,
			outFrame, downFrame, upFrame);
		this.buttonText = this.game.add.text(x + xOffset, y + yOffset, text, textStyle);
	}
}

class MainMenu {
	constructor() {
		this.preload = function () {
			this.game.load.image('background', 'resources/ui/PageBackground.jpg');
			this.game.load.spritesheet('button', 'resources/ui/button_sprite_sheet.png', 623, 104);
		}


		this.create = function () {

			var bg;
			var font = "Cinzel";
			var font_color = "#2e2e2e";
			const btnMap = {
				"Level Select": levelSelect,
				"Controls": controls,
				"About": null,
			};
			var btnList = [];
			var btnX = this.game.world.centerX / 2;
			var btnY = this.game.world.centerY / 2;
			var btnInc = 100;
			var btnTxtXBase = 50;
			var btnTxtY = 30;
			var btnStyle = { font: font, fontSize: "36px", fontWeight: "bold", fill: font_color };

			bg = this.game.add.image(0, 0, 'background');
			bg.width = window.innerWidth;
			bg.height = window.innerHeight;

			var btnYStart = btnY;
			for (const [btnText, onClickFunction] of Object.entries(btnMap)) {
				var btnTxtX = 50 + (btnTxtXBase / btnText.length);
				btnList.push(new MenuButton(btnX, btnYStart, 'button', onClickFunction, this,
					1, 0, 2, 0, btnText, btnStyle, btnTxtX, btnTxtY));
				btnYStart = btnYStart + btnInc;
			}
		}
		this.update = function () {

		}

		function levelSelect() {
			this.game.state.start('levelSelect');
		}
		function controls() {
			this.game.state.start('controls');
		}
		function about() {
			this.game.state.start('about');
		}
	}
}

class Controls {
	constructor() {
		this.preload = function () {
			this.game.load.image('background', 'resources/ui/PageBackground.jpg');
			this.game.load.spritesheet('button', 'resources/ui/button_sprite_sheet.png', 623, 104);
		}

		var bg;
		var font = "Cinzel";
		var font_color = "#2e2e2e";
		const btnMap = {
			"Back": back
		};
		const controlList = [
			"Use WASD to move within the grid",
			"Arrow: Keys Shoot",
			"Arrow: Keys Shoot"
		];
		var btnList = [];
		var btnX = 370;
		var btnY = 250;
		var btnInc = 100;
		var btnTxtXBase = 50;
		var btnTxtY = 30;
		var btnStyle = { font: font, fontSize: "36px", fontWeight: "bold", fill: font_color };
		this.create = function () {
			bg = this.game.add.image(0, 0, 'background');
			bg.width = window.innerWidth;
			bg.height = window.innerHeight;

			var btnYStart = btnY;
			for (var i = 0; i < controlList.length; i++) {
				var btnTxtX = btnTxtXBase;
				this.buttonText = this.game.add.text(btnX + btnTxtX, btnYStart + btnTxtY, controlList[i], btnStyle);
				btnYStart = btnYStart + btnInc;
			}
			for (const [btnText, onClickFunction] of Object.entries(btnMap)) {
				var btnTxtX = 50 + (btnTxtXBase / btnText.length);
				btnList.push(new MenuButton(btnX, btnYStart, 'button', onClickFunction, this,
					1, 0, 2, 0, btnText, btnStyle, btnTxtX, btnTxtY));
				btnYStart = btnYStart + btnInc;
			}
		}
		this.update = function () {

		}

		function back() {
			this.game.state.start('mainMenu');
		}
	}
}

class DeathScreen {
	//Two buttons retry or main menu 
}