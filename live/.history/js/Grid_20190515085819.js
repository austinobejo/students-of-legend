class Grid {
    constructor() {
        this.grid = []
    }

    setPlayer(player) {

        this.player = player;
        this.player.sprite.centerX = this.sprite.centerX;
        this.player.sprite.centerY = this.sprite.centerY;
        this.grid.push(this.player);
    }
    updatePositions() {
        var cellWidth = this.sprite.width / 5;
        var cellHeight = this.sprite.height / 5;

        for (var i = 0; i < this.grid.length; i++) {
            var current = this.grid[i];
            current.sprite.centerX = cellWidth * current.gridX;
            current.sprite.centerY = cellHeight * current.gridY;
        }
    }
}

class GridObject {

    moveTo(x, y) {
        this.gridX = x;
        this.gridY = y;
    }
}

class Player extends GridObject {

    constructor() {
        moveTo(0, 0);
    }
    parseInput() {

        var x = 0;
        var y = 0;

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.W)) {
            y += 1;
        } if (this.game.input.keyboard.isDown(Phaser.Keyboard.S)) {
            y -= 1;
        }
        if (this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            x -= 1;
        } if (this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
            x += 1;
        }

        this.up = this.game.input.keyboard.isDown(Phaser.Keyboard.UP);
        this.down = this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        this.left = this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        this.right = this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
    }
}