class LevelButton {
	constructor(x, y, key, callback, callbackContext, overFrame, outFrame, downFrame, upFrame,
		text, textStyle, xOffset, yOffset, locked) {
		this.game = callbackContext.game;
		this.buttonImage = this.game.add.button(x, y, key, callback, callbackContext, overFrame,
			outFrame, downFrame, upFrame);
		this.buttonText = this.game.add.text(x + xOffset, y + yOffset, text, textStyle);
		if (locked) {
			this.lock = this.game.add.image(x, y, 'lock');
		}
	}
}

class LevelSelect {
	constructor() {
		this.preload = function () {
			this.game.load.image('background', 'resources/ui/PageBackground.jpg');
			this.game.load.image('lock', 'resources/ui/lock.png');
			this.game.load.spritesheet('levelBtn', 'resources/ui/level_button_sprite_sheet.png', 294, 294);
		}

		this.create = function () {

			var bg;
			var font = "Cinzel";
			var font_color = "#2e2e2e";
			const btnMap = {
				"Tutorial": {
					onclick: function () {
						this.game.state.start('tutorial');
					},
					locked: false
				},"Level One: The Pawn": {
					onclick: function () {
						this.game.state.start('level0');
					},
					locked: false
				}
			};
			var btnList = [];
			var btnX = 200;
			var btnY = 250;
			var btnInc = 300;
			var btnTxtXBase = 50;
			var btnTxtY = 30;
			var btnStyle = { font: font, fontSize: "36px", fontWeight: "bold", fill: font_color };

			bg = this.game.add.image(0, 0, 'background');
			bg.width = window.innerWidth;
			bg.height = window.innerHeight;

			var btnXStart = btnX;
			for (const [btnText, btnProp] of Object.entries(btnMap)) {
				var btnTxtX = btnTxtXBase / btnText.length;
				btnList.push(new MenuButton(btnXStart, btnY, 'levelBtn', btnProp.onclick, this,
					1, 0, 2, 0, btnText, btnStyle, btnTxtX, btnTxtY, btnProp.locked));
				btnXStart = btnXStart + btnInc;
			}
		}
		this.update = function () {

		}
	}
}