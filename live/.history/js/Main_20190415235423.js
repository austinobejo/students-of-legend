var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.AUTO, 'Students of Legend', { preload: preload, create: create, update: update, render: render });
game.state.add("mainMenu", new MainMenu());
game.state.add("levelSelect", new LevelSelect());
game.state.add("level0", new Level0());
//game.state.start('level0');

function preload() {
    this.game.load.image('background', 'resources/PageBackground.jpg');
    this.game.load.image('crest', 'resources/crest.png');
} 

var bg;
var crest;
var crest_shadow;
var titleTop;
var titleBottom;
var startPrompt;
var font = "Cinzel";
var font_color = "#2e2e2e";
function create() {
    bg = this.game.add.image(0, 0, 'background');
    bg.width = window.innerWidth;
    bg.height = window.innerHeight;

    crest_shadow = this.game.add.image(100, 100, 'crest');
    crest_shadow.tint = 0;
    crest_shadow.alpha = 0.3;

    crest = this.game.add.image(100, 100, 'crest');

    var topStyle = { font: font, fontSize: "104px", fontWeight:"bold", fill: font_color }
    titleTop = this.game.add.text(450, 150, "STUDENTS of", topStyle);

    var bottomStyle = { font: font, fontSize: "176px", fontWeight:"bold", fill: font_color }
    titleBottom = this.game.add.text(450, 250, "LEGEND", bottomStyle);

    var promptStyle = { font: font, fontSize: "36px", fontWeight:"bold", fill: font_color }
    startPrompt = this.game.add.text(400, 650, "PRESS ANY KEY TO CONTINUE", promptStyle);
    startPrompt.alpha = 0;
    var promptTween = this.game.add.tween(startPrompt).to({ alpha: 1 }, 1000, "Linear", true, 0 , -1);
    promptTween.yoyo(true, 1500);

    this.game.input.keyboard.onPressCallback = function(e) {
    	this.game.input.keyboard.onPressCallback = null;
    	this.game.state.start('mainMenu');
    }
}
function update() {

} 
function render() {

}