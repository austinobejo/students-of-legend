class Tutorial0 {
    constructor() {

        this.centerXOffset = 0;
        this.centerYOffset = 0;
        this.bulletSpeed = 70000;
    }

    preload() {
        this.load.tilemap('base_level', 'resources/maps/base_level.json', null, Phaser.Tilemap.TILED_JSON);
        this.game.load.image('wall', 'resources/maps/wall.png');
        this.game.load.image('floor', 'resources/maps/floor.png');
        this.game.load.image('the_grid', 'resources/maps/the_grid.png');

        this.game.load.image('friendly_bullet', 'resources/players/bullet.png');
        this.game.load.image('player_base', 'resources/players/player_base.png');
        this.game.load.image('king_base', 'resources/enemies/king_base.png');
        this.game.load.image('pawn_base', 'resources/enemies/pawn_base.png');
        this.game.load.image('exit_button', 'resources/ui/exit_button.png');
    }
    create() {
        this.floor = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'floor');
        this.grid = new Grid();
        this.grid.level = this;
        this.grid.sprite = this.game.add.sprite(0, 0, "the_grid");
        this.grid.sprite.centerX = this.game.world.centerX;
        this.grid.sprite.centerY = (this.game.world.centerY + this.game.world.height) / 2;

        this.player = new Player();
        this.player.level = this;
        this.player.sprite = this.game.add.sprite(0, 0, "player_base");
        this.grid.setPlayer(this.player);


        this.boss = new TutorialBoss();
        this.boss.sprite =
            this.cursors = this.game.input.keyboard.createCursorKeys();
        this.graphics = this.game.add.graphics(0, 0);
    }

    getProperty(propertyName, object) {

        for (var i = 0; i < object.properties.length; i++) {

            var prop = object.properties[0];
            if (prop.name == propertyName) {

                return prop.value;
            }
        }
        return null;
    }

    update() {

        this.grid.update();
        this.king.update();
    }

    parseInput() {
        this.player.parseInput();
        this.game.camera.follow(this.player.sprite);
    }

    updateObjects() {
        for (var i = 0; i < this.worldObjects.length; i++) {
            this.worldObjects[i].update();
        }
    }
}
