
class Level0 {
    constructor() {

        this.selectedCharacter = null;
        this.cameraSpeed = 10;
        this.centerXOffset = 0;
        this.centerYOffset = 0;
        this.characterSpeed = 50000;
        this.bulletSpeed = 70000;
    }

    preload() {
        this.load.tilemap('level0', 'resources/maps/level0.json', null, Phaser.Tilemap.TILED_JSON);
        this.game.load.image('wall', 'resources/maps/wall.png');
        this.game.load.image('floor', 'resources/maps/floor.png');

        this.game.load.image('friendly_bullet', 'resources/players/bullet.png');
        this.game.load.image('player', 'resources/players/player.png');
        this.game.load.image('straight_turret', 'resources/enemies/straight_turret.png');
        this.game.load.image('horizontal_turret', 'resources/enemies/horizontal_turret.png');

        this.player = new Player(this.game, this);
        this.player.preload();

        this.game.load.image('exit_button', 'resources/ui/exit_button.png');

        game.load.audio('gunshot', 'resources/audio/gunshot.mp3');
    }
    create() {

        this.exitButton = this.game.add.sprite(window.innerWidth - 128, 0, "exit_button");
        this.exitButton.fixedToCamera = true;
        this.exitButton.inputEnabled = true;
        this.exitButton.events.onInputDown.add(function () { this.game.state.start('mainMenu'); }, this);

        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.map = this.game.add.tilemap('level0');
        this.centerXOffset = this.map.tileWidth / 2;
        this.centerYOffset = this.map.tileHeight / 2;
        this.map.addTilesetImage('wall', 'wall');
        this.map.addTilesetImage('floor', 'floor');

        this.map.createLayer('floor');
        this.collidableLayer = this.map.createLayer('walls');
        this.map.setCollisionBetween(1, 2000, true, 'walls');
        this.collidableLayer.resizeWorld();

        this.centerXOffset = this.map.tileWidth / 2;
        this.centerYOffset = this.map.tileHeight / 2;

        this.game.canvas.oncontextmenu = function (e) { e.preventDefault(); }

        this.player.create(this.map);

        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.graphics = this.game.add.graphics(0, 0);


        this.worldObjectMap = new Map();
        this.worldObjectMap.set(this.player.sprite, this.player);

        this.bulletStockpile = [];
        this.bulletSpriteMap = new Map();
        for (var i = 0; i < 2000; i++) {
            var bulletSprite = this.game.add.sprite(0, 0, "friendly_bullet");
            bulletSprite.visible = false;
            var bulletObject = new Bullet();
            bulletObject.sprite = bulletSprite;
            this.bulletStockpile.push(bulletObject);
            this.game.physics.arcade.enable(bulletSprite);
            bulletObject.sound = this.game.add.audio('gunshot');
            this.bulletSpriteMap.set(bulletObject.sprite, bulletObject);
            this.worldObjectMap.set(bulletObject.sprite, bulletObject);
        }

        this.worldObjects = new Array();
        this.worldObjects.push(this.player)
        this.enemies = new Array();
        this.enemySpriteMap = new Map();

        var player_spawn = this.map.objects["player_spawn"];
        this.player.sprite.x = player_spawn[0].x;
        this.player.sprite.y = player_spawn[0].y;

        for (var i = 0; i < this.map.objects["enemy_spawn"].length; i++) {
            var x = this.map.objects["enemy_spawn"][i];
            var enemy;
            var type = this.getProperty("type", x);
            if (type == "straight_turret") {
                enemy = new StraightTurrent(this.game, this);
                enemy.sprite = this.game.add.sprite(x.x, x.y, "straight_turret");
            } else if (type == "horizontal_turret") {
                enemy = new HorizontalTurrent(this.game, this);
                enemy.sprite = this.game.add.sprite(x.x, x.y, "horizontal_turret");
            }
            enemy.sprite.bringToTop();
            enemy.destination = null;
            enemy.path = [];
            this.game.physics.arcade.enable(enemy.sprite);
            enemy.sprite.body.immovable = true;
            enemy.sprite.body.moves = false;
            this.enemies.push(enemy);
            this.worldObjects.push(enemy);
            this.enemySpriteMap.set(enemy.sprite, enemy);
            this.worldObjectMap.set(enemy.sprite, enemy);
        }
        this.game.camera.deadzone = new Phaser.Rectangle(window.innerWidth / 4, window.innerHeight / 4, 10, 10);
        // var text = "- phaser -\n with a sprinkle of \n pixi dust.";
        // var style = { font: "65px Arial", fill: "#ff0044", align: "center" };

        // var t = this.game.add.text(game.world.centerX - 300, 0, text, style);

    };

    getProperty(propertyName, object) {

        for (var i = 0; i < object.properties.length; i++) {

            var prop = object.properties[0];
            if (prop.name == propertyName) {

                return prop.value
            }
        }
        return null;
    };
    update() {

        this.updatePhysics();
        this.parseInput();
        this.updateObjects();
        this.moveAll();
        this.graphics.clear();
    };

    updatePhysics() {

        for (var i = 0; i < this.worldObjects.length; i++) {
            for (var x = 0; x < this.worldObjects.length; x++) {
                this.game.physics.arcade.collide(this.worldObjects[i].sprite, this.worldObjects[x].sprite);
            }
        }
        for (var i = 0; i < this.worldObjects.length; i++) {
            this.game.physics.arcade.collide(this.worldObjects[i].sprite, this.collidableLayer);
        }

        for (var i = 0; i < this.bulletStockpile.length; i++) {
            this.game.physics.arcade.overlap(this.bulletStockpile[i].sprite, this.collidableLayer, this.onHitThing, null, this);
            for (var x = 0; x < this.worldObjects.length; x++) {
                this.game.physics.arcade.overlap(this.bulletStockpile[i].sprite, this.worldObjects[x].sprite, this.onHitThing, null, this);
            }
            this.game.physics.arcade.overlap(this.bulletStockpile[i].sprite, this.player.shield, this.onHitThing, null, this);
            // for (var x = 0; x < this.bulletStockpile.length; x++) {
            //     this.game.physics.arcade.overlap(this.bulletStockpile[i].sprite, this.bulletStockpile[x].sprite, this.onHitBullet, null, this);
            // }
        }
    }
    onHitThing(c1, c2) {

        var object1 = this.worldObjectMap.get(c1);
        var object2 = this.worldObjectMap.get(c2);
        var bullet;
        var thing;
        if (object1 instanceof Bullet) {
            bullet = object1;
            thing = object2;
        } else {
            bullet = object2;
            thing = object1;
        }

        if (thing instanceof WorldObject) {

            thing.health -= bullet.damage;
        }
        bullet.sprite.visible = false;
        bullet.sprite.body.velocity.x = 0;
        bullet.sprite.body.velocity.y = 0;
        bullet.sprite.x = 0;
        bullet.sprite.y = 0;
    }

    onHitBullet(c1, c2) {

        var bullet1 = this.worldObjectMap.get(c1);
        var bullet2 = this.worldObjectMap.get(c2);

        bullet1.sprite.visible = false;
        bullet1.sprite.body.velocity.x = 0;
        bullet1.sprite.body.velocity.y = 0;
        bullet1.sprite.x = 0;
        bullet1.sprite.y = 0;


        bullet2.sprite.visible = false;
        bullet2.sprite.body.velocity.x = 0;
        bullet2.sprite.body.velocity.y = 0;
        bullet2.sprite.x = 0;
        bullet2.sprite.y = 0;
    }

    parseInput() {
        this.player.parseInput();
        this.game.camera.follow(this.player.sprite);
    }

    updateObjects() {
        for (var i = 0; i < this.worldObjects.length; i++) {
            this.worldObjects[i].update();
        }
    }
    moveAll() {

        for (var i = 0; i < this.enemies.length; i++) {

            if (this.enemies[i].destination != null) {

                if (this.moveTowards(this.enemies[i], this.enemies[i].path[0])) {

                    this.enemies[i].path.shift();
                    if (this.enemies[i].path.length == 0) {

                        this.enemies[i].destination = null;
                    }
                }
            }
        }
    }

    moveTowards(creature, dest) {

        var magnitude = Math.sqrt(Math.pow(creature.sprite.x - dest[0], 2) + Math.pow(creature.sprite.y - dest[1], 2));
        if (magnitude > 1) {
            var directionVectorX = (dest[0] - creature.sprite.x) / magnitude;
            var directionVectorY = (dest[1] - creature.sprite.y) / magnitude;
            creature.sprite.body.velocity.x += directionVectorX * this.characterSpeed * this.game.time.physicsElapsed;
            creature.sprite.body.velocity.y += directionVectorY * this.characterSpeed * this.game.time.physicsElapsed;
            return false;
        } else {
            return true;
        }
    }

    drawLine(lineStart, lineEnd, lineColor = 0xffd900, offsetX = 0, offsetY = 0) {

        this.graphics.lineStyle(1, lineColor, 1);
        this.graphics.moveTo(lineStart[0] + offsetX, lineStart[1] + offsetY);//moving position of graphic if you draw mulitple lines
        this.graphics.lineTo(lineEnd[0] + offsetX, lineEnd[1] + offsetY);
        this.graphics.endFill();
    };

    calculatePath(creature) {
        var targetTileX = creature.destination[0] / this.map.tileWidth;
        var targetTileY = creature.destination[1] / this.map.tileHeight;
        /*
            //Interate though all poossible position the player an occupy. Each position should be at the center of a tile. 
            For each position that we find that is vaoid for the player(not cliding with a wall) add the node and it's edges to the list       
        */
        var allNodeEdges = new Map();

        for (var x = 0; x < this.map.width; x++) {
            for (var y = 0; y < this.map.height; y++) {

                // var edges = [
                //     [-1, -1], [0, -1], [1, -1],
                //     [-1, 0], [1, 0],
                //     [-1, 1], [0, 1], [1, 1]
                // ];

                var edges = [
                    [0, -1],
                    [-1, 0],
                    [1, 0],
                    [0, 1]
                ];
                allNodeEdges.set(String([x, y]), edges);
            }
        }

        //The firmat for things in the openList is [x,y, g, h, shortest path to]

        var allNodes = new Map();

        var playerTileX = Math.floor(creature.sprite.x / this.map.tileWidth);
        var playerTileY = Math.floor(creature.sprite.y / this.map.tileHeight);

        var playerNode = [playerTileX, playerTileY, 0, Phaser.Math.distance(playerTileX, playerTileY, targetTileX, targetTileY), []];
        allNodes.set([playerTileX, playerTileY], playerNode);

        var openList = []
        openList.push(playerNode);
        var closedList = []
        var foundPath = false;
        var finalPath = [];

        while (openList.length > 0 && foundPath == false) {

            openList.sort(function s(a, b) { return (b[2] + b[3]) - (a[2] + a[3]) });

            var current = openList.pop();
            closedList.push(current);

            if (Math.floor(Phaser.Math.distance(current[0], current[1], targetTileX, targetTileY)) == 0) {
                foundPath = true;
                finalPath = current[4];
            }

            var currentEdges = allNodeEdges.get(String([current[0], current[1]]));

            if (currentEdges != null) {
                for (var i = 0; i < currentEdges.length; i++) {

                    var currentEdge = [current[0] + currentEdges[i][0], current[1] + currentEdges[i][1], Number.MAX_SAFE_INTEGER, 0, []];

                    if (allNodes.get(String([currentEdge[0], currentEdge[1]])) == null) {
                        allNodes.set(String([currentEdge[0], currentEdge[1]]), currentEdge);
                        currentEdge[3] = Phaser.Math.distance(currentEdge[0], currentEdge[1], targetTileX, targetTileY);
                    } else {
                        currentEdge = allNodes.get(String([currentEdge[0], currentEdge[1]]));
                    }


                    if (!this.isNodeInArray(currentEdge, closedList) && !this.map.hasTile(currentEdge[0], currentEdge[1], "collidable")/*and if you can actually enter that square (there isnt an obsticale in the way)*/) {
                        if (!this.isNodeInArray(currentEdge, openList)) {
                            openList.push(currentEdge);
                        }
                        if (currentEdge[2] > current[2] + 1) {
                            currentEdge[2] = current[2] + 1;
                            currentEdge[4] = current[4].slice(0);
                            currentEdge[4].push(current);
                        }
                    }
                }
            }
        }

        finalPath.push([targetTileX, targetTileY]);
        finalPath.shift();
        for (var i = 0; i < finalPath.length; i++) {

            finalPath[i][0] = finalPath[i][0] * this.map.tileWidth;
            finalPath[i][1] = finalPath[i][1] * this.map.tileHeight;
        }
        creature.path = finalPath;
    }

    isNodeInArray(node, array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][0] == node[0] && array[i][1] == node[1]) {
                return true;
            }
        }
        return false;
    }

    onPlayerClicked(sprite, pointer) {

        this.selectedCharacter = this.characterSpriteMap.get(sprite);
    };

    onEnemyClicked(sprite, pointer) {

    };
}
