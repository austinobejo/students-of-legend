
class Level0 {
    constructor() {

        this.selectedCharacter = null;
        this.cameraSpeed = 10;
        this.centerXOffset = 0;
        this.centerYOffset = 0;
        this.characterSpeed = 50000;
        this.bulletSpeed = 70000;
    }

    preload() {
        this.load.tilemap('level0', 'resources/maps/level0.json', null, Phaser.Tilemap.TILED_JSON);

        this.game.load.image('wall', 'resources/maps/wall.png');

        this.game.load.image('top_wall', 'resources/maps/top_wall.png');
        this.game.load.image('left_wall', 'resources/maps/left_wall.png');
        this.game.load.image('bottom_wall', 'resources/maps/bottom_wall.png');
        this.game.load.image('right_wall', 'resources/maps/right_wall.png');        
        this.game.load.image('vertical_wall', 'resources/maps/vertical_wall.png');
        this.game.load.image('horizontal_wall', 'resources/maps/horizontal_wall.png');        
        this.game.load.image('top_left_wall', 'resources/maps/top_left_wall.png');
        this.game.load.image('top_right_wall', 'resources/maps/top_right_wall.png');
        this.game.load.image('bottom_left_wall', 'resources/maps/bottom_left_wall.png');
        this.game.load.image('bottom_right_wall', 'resources/maps/bottom_right_wall.png');
        this.game.load.image('floor', 'resources/maps/floor.png');

        this.game.load.image('position_marker', 'resources/maps/position_marker.png');
        this.game.load.image('outline', 'resources/maps/outline.png');

        this.game.load.image('shield', 'resources/misc/shield.png');
        this.game.load.image('bullet', 'resources/misc/shield.png');

        this.game.load.image('gunner', 'resources/players/gunner.png');
        this.game.load.image('shieldbearer', 'resources/players/shieldbearer.png');

        this.game.load.image('turret', 'resources/enemies/turret.png');

        this.game.load.image('exit_button', 'resources/ui/exit_button.png');
    }
    create() {

        this.exitButton = this.game.add.sprite(window.innerWidth - 128, 0, "exit_button");
        this.exitButton.fixedToCamera = true;
        this.exitButton.inputEnabled = true;
        this.exitButton.events.onInputDown.add(function() { this.game.state.start('mainMenu');}, this);

        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        this.map = this.game.add.tilemap('level0');
        this.centerXOffset = this.map.tileWidth / 2;
        this.centerYOffset = this.map.tileHeight / 2;
        this.map.addTilesetImage('top_wall', 'top_wall');
        this.map.addTilesetImage('left_wall', 'left_wall');
        this.map.addTilesetImage('bottom_wall', 'bottom_wall');
        this.map.addTilesetImage('right_wall', 'right_wall');
        this.map.addTilesetImage('vertical_wall', 'vertical_wall');
        this.map.addTilesetImage('horizontal_wall', 'horizontal_wall');
        this.map.addTilesetImage('top_left_wall', 'top_left_wall');
        this.map.addTilesetImage('top_right_wall', 'top_right_wall');
        this.map.addTilesetImage('bottom_left_wall', 'bottom_left_wall');
        this.map.addTilesetImage('bottom_right_wall', 'bottom_right_wall');
        this.map.addTilesetImage('floor', 'floor');

        console.log("asdf");
        this.collidableLayer = this.map.createLayer('walls');
        this.map.createLayer('floor');
        this.map.setCollisionBetween(1, 2000, true, 'walls');
        this.collidableLayer.resizeWorld();

        this.centerXOffset = this.map.tileWidth / 2;
        this.centerYOffset = this.map.tileHeight / 2;

        this.game.canvas.oncontextmenu = function (e) { e.preventDefault(); }

        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.graphics = this.game.add.graphics(0, 0);

        this.position_marker = this.game.add.sprite(0, 0, "position_marker");
        this.position_marker.visible = false;

        this.outline = this.game.add.sprite(0, 0, "outline");
        this.outline.visible = false;

        this.bulletStockpile = [];
        for (var i = 0; i < 1000; i++) {
            var bulletSprite = this.game.add.sprite(0, 0, "bullet");
            bulletSprite.visible = false;
            this.bulletStockpile.push(bulletSprite);
            this.game.physics.arcade.enable(bulletSprite);
        }


        this.characters = new Array();
        this.characterSpriteMap = new Map();
        for (var i = 0; i < this.map.objects["player_spawn"].length; i++) {
            var x = this.map.objects["player_spawn"][i];
            var character = {};
            character.sprite = this.game.add.sprite(x.x, x.y, "test_player");
            character.sprite.bringToTop();
            character.sprite.inputEnabled = true;
            character.sprite.events.onInputDown.add(this.onPlayerClicked, this);
            character.destination = null;
            character.path = [];
            this.game.physics.arcade.enable(character.sprite);
            this.characters.push(character)
            this.characterSpriteMap.set(character.sprite, character);
        }

        this.selectedCharacter = this.characters[0];
        this.enemies = new Array();
        this.enemySpriteMap = new Map();
        for (var i = 0; i < this.map.objects["enemy_spawn"].length; i++) {
            var x = this.map.objects["enemy_spawn"][i];
            var enemy = {};
            enemy.sprite = this.game.add.sprite(x.x, x.y, "test_enemy");
            enemy.sprite.bringToTop();
            enemy.sprite.inputEnabled = true;
            enemy.sprite.events.onInputDown.add(this.onEnemyClicked, this);
            enemy.destination = null;
            enemy.path = [];
            this.game.physics.arcade.enable(enemy.sprite);
            this.enemies.push(enemy)
            this.characterSpriteMap.set(enemy.sprite, enemy);
        }
        this.game.camera.deadzone = new Phaser.Rectangle(window.innerWidth / 4, window.innerHeight / 4, 10, 10);
    };

    update() {

        this.updatePhysics();


        this.parseInput();
        this.moveAll();
        this.graphics.clear();
        this.drawSelectedPathLines();
    };

    updatePhysics() {

        for (var i = 0; i < this.characters.length; i++) {
            for (var x = 0; x < this.enemies.length; x++) {
                this.game.physics.arcade.collide(this.characters[i].sprite, this.enemies[x].sprite);
                this.game.physics.arcade.collide(this.enemies[x].sprite, this.characters[i].sprite);
            }
        }
        for (var i = 0; i < this.characters.length; i++) {
            this.game.physics.arcade.collide(this.characters[i].sprite, this.collidableLayer);
        }
        for (var x = 0; x < this.enemies.length; x++) {
            this.game.physics.arcade.collide(this.enemies[x].sprite, this.collidableLayer);
        }
    }
    parseInput() {

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.ONE)) {
            this.selectedCharacter = this.characters[0];
        } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.TWO)) {
            this.selectedCharacter = this.characters[1];
        }

        if (this.selectedCharacter != null) {

            //If the mouse buttons are down move the character

            this.selectedCharacter.sprite.body.velocity.x = 0;
            this.selectedCharacter.sprite.body.velocity.y = 0;

            if (this.game.input.keyboard.isDown(Phaser.Keyboard.W)) {

                this.selectedCharacter.sprite.body.velocity.y -= this.characterSpeed * this.game.time.physicsElapsed;
            } if (this.game.input.keyboard.isDown(Phaser.Keyboard.S)) {

                this.selectedCharacter.sprite.body.velocity.y += this.characterSpeed * this.game.time.physicsElapsed;
            }

            if (this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {

                this.selectedCharacter.sprite.body.velocity.x -= this.characterSpeed * this.game.time.physicsElapsed;
            } if (this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {

                this.selectedCharacter.sprite.body.velocity.x += this.characterSpeed * this.game.time.physicsElapsed;
            }

            var up = this.game.input.keyboard.isDown(Phaser.Keyboard.UP);
            var down = this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
            var left = this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
            var right = this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)

            if (up && right) {

                this.fireBullet([this.centerXOffset * 1.5, -this.centerYOffset * 1.5], [(this.bulletSpeed * this.game.time.physicsElapsed) / 2, -(this.bulletSpeed * this.game.time.physicsElapsed) / 2]);
            } else if (up && left) {

                this.fireBullet([-this.centerXOffset * 1.5, -this.centerYOffset * 1.5], [-(this.bulletSpeed * this.game.time.physicsElapsed) / 2, -(this.bulletSpeed * this.game.time.physicsElapsed) / 2]);
            } else if (down && left) {

                this.fireBullet([-this.centerXOffset * 1.5, this.centerYOffset * 1.5], [-(this.bulletSpeed * this.game.time.physicsElapsed) / 2, (this.bulletSpeed * this.game.time.physicsElapsed) / 2]);
            } else if (down && right) {

                this.fireBullet([this.centerXOffset * 1.5, this.centerYOffset * 1.5], [(this.bulletSpeed * this.game.time.physicsElapsed) / 2, (this.bulletSpeed * this.game.time.physicsElapsed) / 2]);
            } else if (up) {

                this.fireBullet([this.centerXOffset, this.centerYOffset], [0, -this.bulletSpeed * this.game.time.physicsElapsed]);
            } else if (down) {

                this.fireBullet([0, this.centerYOffset * 2], [0, this.bulletSpeed * this.game.time.physicsElapsed]);
            } else if (left) {

                this.fireBullet([-this.centerXOffset * 2, 0], [-this.bulletSpeed * this.game.time.physicsElapsed, 0]);
            } else if (right) {

                this.fireBullet([this.centerXOffset * 2, 0], [this.bulletSpeed * this.game.time.physicsElapsed, 0]);
            }

            this.outline.visible = true;
            this.outline.bringToTop();
            this.outline.x = this.selectedCharacter.sprite.x;
            this.outline.y = this.selectedCharacter.sprite.y;
            this.game.camera.follow(this.selectedCharacter.sprite);
        } else {

            this.outline.visible = false;
        }


    }

    fireBullet(startPos, velocity) {

        var bullet = this.bulletStockpile.shift();

        bullet.x = this.selectedCharacter.sprite.x + this.centerXOffset;
        bullet.y = this.selectedCharacter.sprite.y + this.centerYOffset;
        bullet.body.velocity.x = velocity[0];
        bullet.body.velocity.y = velocity[1];
        bullet.visible = true;
        this.bulletStockpile.push(bullet);
    }

    moveAll() {

        for (var i = 0; i < this.characters.length; i++) {

            // this.characters[i].sprite.body.velocity.x = 0;
            // this.characters[i].sprite.body.velocity.y = 0;
            if (this.characters[i].destination != null) {

                if (this.characters[i].path.length > 0) {

                    if (this.moveTowards(this.characters[i], this.characters[i].path[0])) {

                        this.characters[i].path.shift();
                        if (this.characters[i].path.length == 0) {

                            this.characters[i].destination = null;
                        }
                    }
                }
            }
        }

        for (var i = 0; i < this.enemies.length; i++) {

            if (this.enemies[i].destination != null) {

                if (this.moveTowards(this.enemies[i], this.enemies[i].path[0])) {

                    this.enemies[i].path.shift();
                    if (this.enemies[i].path.length == 0) {

                        this.enemies[i].destination = null;
                    }
                }
            }
        }
    }

    moveTowards(creature, dest) {

        var magnitude = Math.sqrt(Math.pow(creature.sprite.x - dest[0], 2) + Math.pow(creature.sprite.y - dest[1], 2));
        if (magnitude > 1) {
            console.log(magnitude);
            var directionVectorX = (dest[0] - creature.sprite.x) / magnitude;
            var directionVectorY = (dest[1] - creature.sprite.y) / magnitude;
            creature.sprite.body.velocity.x += directionVectorX * this.characterSpeed * this.game.time.physicsElapsed;
            creature.sprite.body.velocity.y += directionVectorY * this.characterSpeed * this.game.time.physicsElapsed;
            return false;
        } else {
            return true;
        }
    }

    drawSelectedPathLines() {
        for (var i = 0; i < this.characters.length; i++) {

            if (this.characters[i].destination != null) {
                this.calculatePath(this.characters[i]);

                if (this.characters[i].path.length > 0) {

                    var prev = this.characters[i].path[0];
                    this.drawLine([this.characters[i].sprite.x, this.characters[i].sprite.y], prev, 0xffd900, this.centerXOffset, this.centerYOffset);

                    for (var x = 1; x < this.characters[i].path.length; x++) {

                        this.drawLine(prev, this.characters[i].path[x], 0xffd900, this.centerXOffset, this.centerYOffset);
                        prev = this.characters[i].path[x];
                    }
                }
            }
        }
    }
    drawLine(lineStart, lineEnd, lineColor = 0xffd900, offsetX = 0, offsetY = 0) {

        this.graphics.lineStyle(1, lineColor, 1);
        this.graphics.moveTo(lineStart[0] + offsetX, lineStart[1] + offsetY);//moving position of graphic if you draw mulitple lines
        this.graphics.lineTo(lineEnd[0] + offsetX, lineEnd[1] + offsetY);
        this.graphics.endFill();
    };

    calculatePath(creature) {
        var targetTileX = creature.destination[0] / this.map.tileWidth;
        var targetTileY = creature.destination[1] / this.map.tileHeight;
        /*
            //Interate though all poossible position the player an occupy. Each position should be at the center of a tile. 
            For each position that we find that is vaoid for the player(not cliding with a wall) add the node and it's edges to the list       
        */
        var allNodeEdges = new Map();

        for (var x = 0; x < this.map.width; x++) {
            for (var y = 0; y < this.map.height; y++) {

                // var edges = [
                //     [-1, -1], [0, -1], [1, -1],
                //     [-1, 0], [1, 0],
                //     [-1, 1], [0, 1], [1, 1]
                // ];

                var edges = [
                    [0, -1],
                    [-1, 0],
                    [1, 0],
                    [0, 1]
                ];
                allNodeEdges.set(String([x, y]), edges);
            }
        }

        //The firmat for things in the openList is [x,y, g, h, shortest path to]

        var allNodes = new Map();

        var playerTileX = Math.floor(creature.sprite.x / this.map.tileWidth);
        var playerTileY = Math.floor(creature.sprite.y / this.map.tileHeight);

        var playerNode = [playerTileX, playerTileY, 0, Phaser.Math.distance(playerTileX, playerTileY, targetTileX, targetTileY), []];
        allNodes.set([playerTileX, playerTileY], playerNode);

        var openList = []
        openList.push(playerNode);
        var closedList = []
        var foundPath = false;
        var finalPath = [];

        while (openList.length > 0 && foundPath == false) {

            openList.sort(function s(a, b) { return (b[2] + b[3]) - (a[2] + a[3]) });

            var current = openList.pop();
            closedList.push(current);

            if (Math.floor(Phaser.Math.distance(current[0], current[1], targetTileX, targetTileY)) == 0) {
                foundPath = true;
                finalPath = current[4];
            }

            var currentEdges = allNodeEdges.get(String([current[0], current[1]]));

            if (currentEdges != null) {
                for (var i = 0; i < currentEdges.length; i++) {

                    var currentEdge = [current[0] + currentEdges[i][0], current[1] + currentEdges[i][1], Number.MAX_SAFE_INTEGER, 0, []];

                    if (allNodes.get(String([currentEdge[0], currentEdge[1]])) == null) {
                        allNodes.set(String([currentEdge[0], currentEdge[1]]), currentEdge);
                        currentEdge[3] = Phaser.Math.distance(currentEdge[0], currentEdge[1], targetTileX, targetTileY);
                    } else {
                        currentEdge = allNodes.get(String([currentEdge[0], currentEdge[1]]));
                    }


                    if (!this.isNodeInArray(currentEdge, closedList) && !this.map.hasTile(currentEdge[0], currentEdge[1], "collidable")/*and if you can actually enter that square (there isnt an obsticale in the way)*/) {
                        if (!this.isNodeInArray(currentEdge, openList)) {
                            openList.push(currentEdge);
                        }
                        if (currentEdge[2] > current[2] + 1) {
                            currentEdge[2] = current[2] + 1;
                            currentEdge[4] = current[4].slice(0);
                            currentEdge[4].push(current);
                        }
                    }
                }
            }
        }

        finalPath.push([targetTileX, targetTileY]);
        finalPath.shift();
        for (var i = 0; i < finalPath.length; i++) {

            finalPath[i][0] = finalPath[i][0] * this.map.tileWidth;
            finalPath[i][1] = finalPath[i][1] * this.map.tileHeight;
        }
        creature.path = finalPath;
    }

    isNodeInArray(node, array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][0] == node[0] && array[i][1] == node[1]) {
                return true;
            }
        }
        return false;
    }

    onPlayerClicked(sprite, pointer) {

        this.selectedCharacter = this.characterSpriteMap.get(sprite);
    };

    onEnemyClicked(sprite, pointer) {

    };
}
