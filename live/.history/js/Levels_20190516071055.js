class BaseLevel {
    constructor() {

        this.centerXOffset = 0;
        this.centerYOffset = 0;
        this.bulletSpeed = 70000;
    }

    preload() {
        this.game.load.image('wall', 'resources/maps/wall.png');
        this.game.load.image('floor', 'resources/maps/floor.png');
        this.game.load.image('the_grid', 'resources/maps/the_grid.png');

        this.game.load.image('player_base', 'resources/characters/player_base.png');
        this.game.load.image('player_damaged_1', 'resources/characters/player_damaged_1.png');
        this.game.load.image('player_damaged_2', 'resources/characters/player_damaged_2.png');
        this.game.load.image('player_invinsible', 'resources/characters/player_invinsible.png');
        this.game.load.image('player_dead', 'resources/characters/player_dead.png');
        this.game.load.image('player_attack_base', 'resources/characters/player_attack_base.png');
        this.game.load.image('player_attack_finish', 'resources/characters/player_attack_finish.png');

        this.game.load.image('pawn_base', 'resources/characters/pawn_base.png');

        this.game.load.image('knight_base', 'resources/characters/knight_base.png');

        this.game.load.image('enemy_attack_finish', 'resources/characters/enemy_attack_finish.png');
        this.game.load.image('attack_marker_base', 'resources/characters/attack_marker_base.png');
    }
    create() {
        this.floor = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'floor');
        this.grid = new Grid();
        this.grid.level = this;
        this.grid.sprite = this.game.add.sprite(0, 0, "the_grid");
        this.grid.sprite.centerX = this.game.world.centerX;
        this.grid.sprite.centerY = (this.game.world.centerY + this.game.world.height) / 2;
        this.grid.create();

        this.player = new Player();
        this.player.level = this;
        this.player.sprite = this.game.add.sprite(0, 0, "player_base");
        this.player.attackWarpupSprite = this.game.add.sprite(0, 0, "player_attack_base");
        this.player.attackWarpupSprite.visible = false;
        this.player.attackSprite = this.game.add.sprite(0, 0, "player_attack_finish");
        this.player.attackSprite.visible = false;
        this.player.damaged1Sprite = this.game.add.sprite(0, 0, "player_damaged_1");
        this.player.damaged1Sprite.visible = false;
        this.player.damaged2Sprite = this.game.add.sprite(0, 0, "player_damaged_2");
        this.player.damaged2Sprite.visible = false;
        this.player.invinisibleSprite = this.game.add.sprite(0, 0, "player_invinsible");
        this.player.invinisibleSprite.visible = false;
        this.player.deadSprite = this.game.add.sprite(0, 0, "player_dead");
        this.player.deadSprite.visible = false;
        this.grid.setPlayer(this.player);

        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.graphics = this.game.add.graphics(0, 0);
    }
    update() {
        var down = this.level.game.input.keyboard.isDown(Phaser.Keyboard.ESCAPE);
        if (down) {
            this.game.state.start('levelSelect ');
        }
        this.player.update();
        this.boss.update();
        this.grid.update();
    }
}
class Tutorial extends BaseLevel {
    preload() {
        super.preload();
        this.game.load.image('helper_base', 'resources/characters/helper_base.png');
    }
    create() {
        super.create()
        this.boss = new TutorialBoss();
        this.boss.level = this;
        this.boss.sprite = this.game.add.sprite(0, 0, "helper_base");
        this.boss.sprite.centerX = this.game.world.centerX;
        this.boss.sprite.centerY = this.game.world.centerY * .33;
        this.boss.create();
    }
}

class Level1 extends BaseLevel {
    preload() {
        super.preload();
        this.game.load.image('pawn_boss_base', 'resources/characters/pawn_boss_base.png');
    }
    create() {
        super.create()
        this.boss = new PawnBoss();
        this.boss.level = this;
        this.boss.sprite = this.game.add.sprite(0, 0, "pawn_boss_base");
        this.boss.sprite.centerX = this.game.world.centerX;
        this.boss.sprite.centerY = this.game.world.centerY * .36;
        this.boss.create();
    }
}

class Level3 extends BaseLevel {
    preload() {
        super.preload();
        this.game.load.image('king_base', 'resources/characters/king_base.png');
    }
    create() {
        super.create()
        this.boss = new King();
        this.boss.level = this;
        this.boss.sprite = this.game.add.sprite(0, 0, "king_base");
        this.boss.sprite.centerX = this.game.world.centerX;
        this.boss.sprite.centerY = this.game.world.centerY * .33;
        this.boss.create();
    }
}