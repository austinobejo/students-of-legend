var game = new Phaser.Game(window.innerWidth , window.innerHeight, Phaser.AUTO, 'Students of Legend', { preload: preload, create: create, update: update, render: render });
game.state.add("levelSelect", new LevelSelect());
game.state.add("controls", new Controls());
game.state.add("credits", new Credits());
game.state.add("tutorial", new Tutorial());
game.state.add("level1", new Level1());
game.state.add("level3", new Level3());
// game.state.start('levelSelect');
game.state.start('main');
// game.state.start('tutorial0');
// game.state.start('level1');
// game.state.start('level3');

function preload() {
    this.game.load.image('background', 'resources/ui/PageBackground.jpg');
    this.game.load.image('crest', 'resources/ui/crest.png');
    this.game.load.audio('welcome', 'resources/audio/welcome.mp3');
} 

var bg;
var crest;
var crest_shadow;
var titleTop;
var titleBottom;
var startPrompt;
var font = "Cinzel";
var font_color = "#2e2e2e";
var welcome_sound;
function create() {
    bg = this.game.add.image(0, 0, 'background');
    bg.width = window.innerWidth;
    bg.height = window.innerHeight;
   
    var topStyle = { font: font, fontSize: "104px", fontWeight:"bold", fill: font_color }
    titleTop = this.game.add.text(450, 150, "Student of Legend", topStyle);
    titleTop.centerX = this.game.world.centerX;
    titleTop.centerY = this.game.world.centerY + 100;

    crest = this.game.add.image(100, 100, 'crest');
    crest.bottom = titleTop.top;
    crest.centerX = this.game.world.centerX;

    var promptStyle = { font: font, fontSize: "36px", fontWeight:"bold", fill: font_color }
    startPrompt = this.game.add.text(400, 650, "PRESS ANY KEY TO CONTINUE", promptStyle);
    startPrompt.centerX = this.game.world.centerX;
    startPrompt.centerY = titleTop.bottom + 50;

    startPrompt.alpha = 0;
    var promptTween = this.game.add.tween(startPrompt).to({ alpha: 1 }, 1000, "Linear", true, 0 , -1);
    promptTween.yoyo(true, 1500);

    this.game.input.keyboard.onPressCallback = function(e) {
    	this.game.input.keyboard.onPressCallback = null;
    	this.game.state.start('levelSelect');
    }

    welcome_sound = this.game.add.audio('welcome');
    this.game.sound.setDecodedCallback([welcome_sound], playSound);  
}
function playSound() {
    welcome_sound.volume = 1;
    welcome_sound.loopFull(0.6);
}
function update() {

} 
function render() {

}