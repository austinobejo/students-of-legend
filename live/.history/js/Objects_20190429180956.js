class WorldObject{
    constructor(game) {
        this.game = game;
        this.characterSpeed = 50000;
        this.weapon = null;
    }
    preload();
    create(map);
}

class Player extends WorldObject {

    preload() {

        this.game.load.image('gunner', 'resources/players/gunner.png');
    }
    create(map) {

        this.cursors = this.game.input.keyboard.createCursorKeys();
    }
}

class Enemy extends WorldObject{

    
}

class StraightTurrent extends Enemy {

    preload() {

        this.game.load.image('turret', 'resources/enemies/turret.png');
    }
}

class Weapon {

    constructor(sprite) {
        this.sprite = sprite
    }
    shoot(direction) {

    };
}

class Bullet {
    constructor(position, color) {
        this.position = position;
        this.color = color;
    }

    onHit() {
        
    }
}