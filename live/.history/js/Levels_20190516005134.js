class BaseLevel {
    constructor() {

        this.centerXOffset = 0;
        this.centerYOffset = 0;
        this.bulletSpeed = 70000;
    }

    preload() {
        this.game.load.image('wall', 'resources/maps/wall.png');
        this.game.load.image('floor', 'resources/maps/floor.png');
        this.game.load.image('the_grid', 'resources/maps/the_grid.png');
        this.game.load.image('attack_marker_base', 'resources/maps/attack_marker_base.png');

        this.game.load.image('player_base', 'resources/characters/player_base.png');
        this.game.load.image('player_attack_base', 'resources/characters/player_attack_base.png');
        this.game.load.image('player_attack_empty', 'resources/characters/player_attack_empty.png');

        this.game.load.image('king_base', 'resources/characters/king_base.png');

        this.game.load.image('pawn_base', 'resources/characters/pawn_base.png');
        this.game.load.image('pawn_grid', 'resources/characters/pawn_grid.png');

        this.game.load.image('helper_base', 'resources/characters/helper_base.png');


        // this.game.load.bitmapFont('desyrel', 'assets/fonts/bitmapFonts/desyrel.png', 'assets/fonts/bitmapFonts/desyrel.xml');
    }
    create() {
        this.floor = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'floor');
        this.grid = new Grid();
        this.grid.level = this;
        this.grid.sprite = this.game.add.sprite(0, 0, "the_grid");
        this.grid.sprite.centerX = this.game.world.centerX;
        this.grid.sprite.centerY = (this.game.world.centerY + this.game.world.height) / 2;
        this.grid.create();

        this.player = new Player();
        this.player.level = this;
        this.player.sprite = this.game.add.sprite(0, 0, "player_base");
        this.player.attackWarpupSprite = this.game.add.sprite(0, 0, "player_attack_empty");
        this.player.attackWarpupSprite.visible = false;
        this.player.attackSprite = this.game.add.sprite(0, 0, "player_attack_base");
        this.player.attackSprite.visible = false;
        this.grid.setPlayer(this.player);
        
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.graphics = this.game.add.graphics(0, 0);
    }
    update() {

        this.player.update();
        this.boss.update();
        this.grid.update();
    }
}
class Tutorial0 extends BaseLevel {

    create() {
        super.create()
        this.boss = new TutorialBoss();
        this.boss.level = this;
        this.boss.sprite = this.game.add.sprite(0, 0, "helper_base");
        this.boss.sprite.centerX = this.game.world.centerX;
        this.boss.sprite.centerY = this.game.world.centerY * .33;
        this.boss.create();
    }
}


class Level1 extends BaseLevel {

    create() {
        super.create()
        this.king = new King();
        this.king.level = this;
        this.king.sprite = this.game.add.sprite(0, 0, "king_base");
        this.king.sprite.centerX = this.game.world.centerX;
        this.king.sprite.centerY = this.game.world.centerY * .33;
        this.king.create();
    }
}