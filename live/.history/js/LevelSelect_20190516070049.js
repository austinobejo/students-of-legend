class LevelButton {
	constructor(x, y, key, callback, callbackContext, overFrame, outFrame, downFrame, upFrame,
		text, textStyle, xOffset, yOffset, locked) {
		this.game = callbackContext.game;
		this.buttonImage = this.game.add.button(x, y, key, callback, callbackContext, overFrame,
			outFrame, downFrame, upFrame);
		this.buttonText = this.game.add.text(x + xOffset, y + yOffset, text, textStyle);
		if (locked) {
			this.lock = this.game.add.image(x, y, 'lock');
		}
	}
}

class LevelSelect {
	constructor() {
		this.preload = function () {
			this.game.load.image('background', 'resources/ui/PageBackground.jpg');
			this.game.load.image('lock', 'resources/ui/lock.png');
			this.game.load.spritesheet('levelBtn', 'resources/ui/level_button_sprite_sheet.png', 294, 294);
		}

		this.create = function () {



			var bg;
			bg = this.game.add.image(0, 0, 'background');
			bg.width = window.innerWidth;
			bg.height = window.innerHeight;

			const btnMap = {
				"Tutorial":
					function () {
						this.game.state.start('tutorial');
					}
				, "Level One: The Pawn":
					function () {
						this.game.state.start('level1');
					}

			};

			// var btnStyle = { font: "Cinzel", fontSize: "36px", fontWeight: "bold", fill: "#2e2e2e" };
			// var tutorialButton = new MenuButton(0, 0, 'levelBtn',
			// 	function () {
			// 		this.game.state.start('tutorial');
			// 	},
			// 	this, 1, 0, 2, 0, Tutorial, btnStyle, 0, 0, false)

			var buttonImage = this.game.add.button(0, 0, 'levelBtn', function () {
				this.game.state.start('tutorial');
			}
				, this, 1, 0, 2, 0);

			buttonImage.centerX = this.game.world.centerX;
			buttonImage.centerY = this.game.world.centerY / 2;

			var style = { font: "36px Cinzel", fill: "#000000", align: "center" };
			var buttonText = this.game.add.text(0, 0, "Tutorial", style);
			buttonText.centerX = buttonImage.centerX;
			buttonText.centerY = buttonImage.centerY;
			buttonText.bringToTop();

			// var btnList = [];
			// var btnX = 200;
			// var btnY = 250;
			// var btnInc = 300;
			// var btnTxtXBase = 50;
			// var btnTxtY = 30;


			// var btnXStart = btnX;
			// for (const [btnText, btnProp] of Object.entries(btnMap)) {
			// 	var btnTxtX = btnTxtXBase / btnText.length;
			// 	btnList.push();
			// 	btnXStart = btnXStart + btnInc;
			// }
		}
		this.update = function () {

		}
	}
}